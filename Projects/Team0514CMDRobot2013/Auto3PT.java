/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.mphs.first.wpilibj.commands.Shoot;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;

/**
 *
 * @author marnold
 */
public class Auto3PT extends CommandGroup {
    
    public Auto3PT() {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.
        addParallel(new Set3PTAngle());
        addSequential(new Set3PTShot());
        addSequential(new WaitCommand(3.0));
        addSequential(new Shoot());
        addSequential(new WaitCommand(3.0));
        addSequential(new Shoot());
        
        
    }
}
