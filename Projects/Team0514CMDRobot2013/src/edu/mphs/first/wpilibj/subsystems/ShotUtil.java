/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.DontShoot;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author Team 514
 */
public class ShotUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    Solenoid m_firepin;
    public ShotUtil(){
        m_firepin= new Solenoid(RobotMap.FIRE_PIN);  
    }
    public void shoot(){
        m_firepin.set(true);
    }
    public void dontshoot(){
        m_firepin.set(false);
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new DontShoot());
    }
}
