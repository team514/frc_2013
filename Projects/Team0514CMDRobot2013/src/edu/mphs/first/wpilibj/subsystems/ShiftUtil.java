/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.ShiftHigh;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


/**
 *
 * @author marnold
 */
public class ShiftUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    Solenoid shifter;

    public ShiftUtil() {
        shifter = new Solenoid(RobotMap.Shifter);
    }
    
    public boolean getGear(){
        return shifter.get();
    }
    
    public void shift(boolean gear) {
        shifter.set(gear);
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new ShiftHigh());
    }
    
    public void updateStatus(){
        SmartDashboard.putBoolean("Shifter (True=High//False=Low)", shifter.get());
    }
}
