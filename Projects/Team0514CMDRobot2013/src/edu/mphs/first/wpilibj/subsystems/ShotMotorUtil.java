/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class ShotMotorUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    SpeedController shooter;
    double speed;
    int range;
    boolean off, low, mid, high;
    
    public ShotMotorUtil(){
        shooter = new Jaguar(RobotMap.SHOOTER);
        speed = 0.0;
        range = 0;
        off = true;
        low = false;
        mid = false;
        high = false;
    }
    
    public double getSpeed(){
        return this.speed;
    }
    
    public int getRange(){
        return this.range;
    }
    
    public void setRange(int range){
        switch (range){
            case 0: {  //Off
                setSpeed(0.0);
                this.range = 0;
                break;
            }
            case 1: {  //Low
                setSpeed(RobotMap.lowRate);
                this.range = 1;
                break;
            }
            case 2: {  //Mid
                setSpeed(RobotMap.midRate);
                this.range = 2;
                break;
            }
            case 3: {  //High
                setSpeed(RobotMap.highRate);
                this.range = 3;
                break;
            }
            default:{
                this.range = checkRange(range);
                break;
            }
        }
    }
    
    public void setSpeed(double speed){
        this.speed = checkMinMax(speed);
        shooter.set(this.speed);
    }
    
    private double checkMinMax(double speed){
        if(speed > RobotMap.highRate){
            speed = RobotMap.highRate;
            this.range = 3;
        }
        if(speed < 0.0){
            speed = 0.0;
            this.range = 0;
        }
        return speed;
    }
    private int checkRange(int range){
        if(range < 0){
            range = 0;
            setSpeed(0.0);
        }
        if(range > 3){
            range = 3;
            setSpeed(RobotMap.highRate);
        }
        return range;
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
    public void updateStatus() {
        SmartDashboard.putNumber("ShotSpeed", shooter.get());
        SmartDashboard.putNumber("Shot Motor Scale", shooter.get());
    }
}
