/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.StopAngleMotor;
import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class ShotAngleUtil extends Subsystem {

    SpeedController angleMotor;
    AnalogChannel pot;
    int range;
    double point;
    boolean onTarget, angleMode;
    
    
    // Initialize your subsystem here
    public ShotAngleUtil() {
        
        angleMotor = new Jaguar(RobotMap.ANGLE);
        pot = new AnalogChannel(RobotMap.POT1_CHANNEL);
        
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new StopAngleMotor());
    }
    
    public boolean isOnTarget(){
        return this.onTarget;
    }
    
    public double getPosition(){
        return pot.pidGet();
    }
    
    public void stop(){
        angleMotor.set(0.00);
    }
    
    private void moveAngle(){
        if(getPosition() <= this.point + 5 && getPosition() >= this.point - 5){
            angleMotor.set(0.00);
            this.onTarget = true;
        }else{
            this.onTarget = false;
        }
        
        if(!this.onTarget){
            if(getPosition() < this.point){
                angleMotor.set(RobotMap.AngleDownSetSpeed);
            }
            if(getPosition() > this.point){
                angleMotor.set(RobotMap.AngleUpSetSpeed);
            }
        }
    }
    
    public void setPosition(double setpoint){  //Bot = 485, and Top = 330
        if(checkSetpoint(setpoint)){
                this.point = setpoint;
                moveAngle();
        }
    }
    
    public int getRange(){
        return this.range;
    }
    
    private boolean checkSetpoint(double setpoint){
        boolean status = false;
        if((setpoint < RobotMap.SPC_In_Bot) &&
          (setpoint > RobotMap.SPC_In_Top)){
                status = true;            
        }
        return status;
    }
    
    public void setRange(int range){
          switch (range){
            case 0: {  //Off
                this.point = RobotMap.lowangle;
                this.range = 0;
                break;
            }
            case 1: {  //Low
                this.point = RobotMap.lowangle;
                this.range = 1;
                break;
            }
            case 2: {  //Mid
                this.point = RobotMap.midangle;
                this.range = 2;
                break;
            }
            case 3: {  //High
                this.point = RobotMap.highangle;
                this.range = 3;
                break;
            }
            default:{
                this.range = checkRange(range);
                break;
            }
        }
        moveAngle();
    }
    
    private int checkRange(int range){
        if(range < 0){
            range = 0;
            this.point = RobotMap.lowangle;
        }
        if(range > 3){
            range = 3;
            this.point = RobotMap.highangle;
        }        
        return range;
    }
    public void updateStatus(){
        SmartDashboard.putBoolean("Angle Mode", this.angleMode);
        SmartDashboard.putNumber("Shot Angle Value",getPosition());
        SmartDashboard.putNumber("Shot Angle Scale", getPosition());
    }
}
