/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.DriveInTank;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author marnold
 */
public class DriveUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    RobotDrive drive;
    boolean drivemode;
    
    public DriveUtil(){
        drive = new RobotDrive(RobotMap.LEFT_JAGUAR, RobotMap.RIGHT_JAGUAR);
        drivemode = true;
        drive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    public void driveTank(double leftY, double rightY){
        drive.tankDrive(leftY, rightY);
        setDrivemode(true);
    }
    
    public void driveTank(Joystick left, Joystick right) {
        drive.tankDrive(left, right);
        setDrivemode(true);
    }
    
    public void driveArcade(double rightY, double rightX) {
        drive.arcadeDrive(rightY, rightX);
        setDrivemode(false);
    }
    
    public void driveArcade(Joystick right){
        drive.arcadeDrive(right);
        setDrivemode(false);
    }
    
    public boolean getDrivemode(){
        return this.drivemode;
    }
    
    private void setDrivemode(boolean drivemode) {
        this.drivemode = drivemode;
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        
        setDefaultCommand(new DriveInTank());
    }
}
