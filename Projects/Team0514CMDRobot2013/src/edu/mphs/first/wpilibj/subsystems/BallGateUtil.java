/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.ManageBallGate;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author marnold
 */
public class BallGateUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    DoubleSolenoid ballgate;
    DigitalInput lswitch;
    
    public BallGateUtil(){
        ballgate = new DoubleSolenoid(RobotMap.gateClosed, RobotMap.gateOpen);
        lswitch = new DigitalInput(RobotMap.lSwitchChannel);
}
    public void manageGate() {
        if(lswitch.get()){
            activateGate();
        }else{
            deactivateGate();
        }
    }
    
    public void activateGate() {
        ballgate.set(DoubleSolenoid.Value.kForward);
    }
    
    public void deactivateGate() {
        ballgate.set(DoubleSolenoid.Value.kReverse);
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new ManageBallGate());
    }
}
