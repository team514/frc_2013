/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.BallMotorOff;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class BallUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    SpeedController ballMotor;
    boolean state;
    
    public BallUtil (){
        ballMotor = new Jaguar(RobotMap.BALL_JAGUAR);
        state = false;
    }
    
    public boolean getState(){
        return this.state;
    }
    
    public void activateBallMotor(){
        ballMotor.set(1);
        this.state = true;
    }
    
    public void deactivateBallMotor(){
        ballMotor.set(0);
        this.state = false;
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new BallMotorOff());
    }

}
