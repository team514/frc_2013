/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.RetractArm;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author Team 514
 */
public class BridgeUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    DoubleSolenoid m_Arm;
    
    public BridgeUtil() {
        //instantiate objeccts here
        m_Arm = new DoubleSolenoid(RobotMap.Arm_Back, RobotMap.Arm_Out);
    }
    public void armsOut(){
          m_Arm.set(DoubleSolenoid.Value.kReverse);
    }
    public void armsBack(){
          m_Arm.set(DoubleSolenoid.Value.kForward);
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new RetractArm());
    }
}
