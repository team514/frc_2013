/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.StartCompressor;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author marnold
 */
public class CompressorUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

    Compressor comp;
    
    public CompressorUtil() {
        comp = new Compressor(RobotMap.COMP_P_SWITCH_CHANNEL, RobotMap.COMP_C_RELAY_CHANNEL);
    }
    
    public void startComp() {
        comp.start();
    }
    
    public boolean getState(){
        return comp.enabled();
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new StartCompressor());
    }
}
