/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj;

/**
 *
 * @author marnold
 */
import com.sun.squawk.util.MathUtils;
import edu.mphs.first.wpilibj.commands.SetKinectAngle;
import edu.mphs.first.wpilibj.commands.SetKinectShotSpeed;
import edu.mphs.first.wpilibj.commands.Shoot;
import edu.wpi.first.wpilibj.Kinect;
import edu.wpi.first.wpilibj.Skeleton;
import edu.wpi.first.wpilibj.buttons.Button;


/**
 * This class is the glue that binds the Kinect controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */

public class OIK {
// Variables for KinectOI processing Only
        Kinect m_kinect;
        double leftAxis = 0;
        double rightAxis = 0;
        double leftAngle, rightAngle, headAngle, rightLegAngle, leftLegAngle, rightLegYZ, leftLegYZ;
        boolean dataWithinExpectedRange;
        Button headLeft;
        Button headRight;
        Button leftLegOut;
        Button rightLegOut;
        Button rightLegForward;
        Button rightLegBackward;
        Button leftLegForward;
        Button leftLegBackward;
        Button rightAngleButton;
        Button leftAngleButton;
        
        
    
  public OIK(){
        //Get Instance of KinectOI from Kinect Server
        m_kinect = Kinect.getInstance();
        
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    //leftStick = new Joystick(RobotMap.LEFT_JOYSTICK);
    
    rightAngleButton = new Button() {

            public boolean get() {
               if(getRightAxis() > 0){
                  return true;
               }else{
                  return false;
               }
            }
        };
    leftAngleButton = new Button() {

            public boolean get() {
                if(getLeftAxis() > 0){
                    return true;
                }else{
                    return false;
                }
            }
    };
    headLeft = new Button() {

            public boolean get() {
                return getHeadLeft();
            }
    };
    headRight = new Button() {

            public boolean get() {
                return getHeadRight();
            }
    };
    leftLegForward = new Button() {

            public boolean get() {
                return getLeftLegForward();
            }
    };
    rightLegForward = new Button() {

            public boolean get() {
                return getRightLegForward();
            }
    };
        
    }
    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);
  
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
    
    
    public double getRightAxis(){
        return rightAxis;
    }
    
    public double getLeftAxis(){
        return leftAxis;
    }
    
    private boolean getHeadLeft(){
        return true;
    }
    
    private boolean getHeadRight(){
        return true;
    }
    
    private boolean getRightLegForward(){
        return true;
    }
    
    private boolean getLeftLegForward(){
        return true;
    }
    
    public void mainKinectOI(){
                        /* Determine angle of each arm and map to range -1,1 */
                leftAngle = AngleXY(m_kinect.getSkeleton().GetShoulderLeft(), m_kinect.getSkeleton().GetWristLeft(), true);
                rightAngle = AngleXY(m_kinect.getSkeleton().GetShoulderRight(), m_kinect.getSkeleton().GetWristRight(), false);
                leftAxis = CoerceToRange(leftAngle, -70, 70, -1, 1);
                rightAxis = CoerceToRange(rightAngle, -70, 70, -1, 1);
                /* Check if arms are within valid range and at approximately the same z-value */
                dataWithinExpectedRange = leftAngle < RobotMap.ARM_MAX_ANGLE && 
                                          leftAngle > RobotMap.ARM_MIN_ANGLE &&
                                          rightAngle < RobotMap.ARM_MAX_ANGLE && 
                                          rightAngle > RobotMap.ARM_MIN_ANGLE;
                dataWithinExpectedRange = dataWithinExpectedRange &&
                                          InSameZPlane(m_kinect.getSkeleton().GetShoulderLeft(),
                                                   m_kinect.getSkeleton().GetWristLeft(),
                                                   RobotMap.Z_PLANE_TOLERANCE) &&
                                          InSameZPlane(m_kinect.getSkeleton().GetShoulderRight(),
                                                   m_kinect.getSkeleton().GetWristRight(),
                                                   RobotMap.Z_PLANE_TOLERANCE);

                /* Determine the head angle and use it to set the Head buttons */
                if(dataWithinExpectedRange){
                    rightAngleButton.whenPressed(new SetKinectShotSpeed());
                    
                    if(leftAngle >= RobotMap.ARM_TRIGGER_ANGLE){
                        leftAngleButton.whenPressed(new SetKinectAngle());
                    }
//                    leftAngleButton.whenPressed(new SetKinectShotAngle());
                    headAngle = AngleXY(m_kinect.getSkeleton().GetShoulderCenter(), m_kinect.getSkeleton().GetHead(), false);
                    if(headAngle > RobotMap.HEAD_LEFT){
                        //headLeft.whenPressed(new DeployArm());
                    }
                    if(headAngle < RobotMap.HEAD_RIGHT){
                       // headRight.whenPressed(new RetractArm());
                    }
                    /* Calculate the leg angles in the XY plane and use them to set the Leg Out buttons */
                    leftLegAngle = AngleXY(m_kinect.getSkeleton().GetHipLeft(), m_kinect.getSkeleton().GetAnkleLeft(), true);
                    rightLegAngle = AngleXY(m_kinect.getSkeleton().GetHipRight(), m_kinect.getSkeleton().GetAnkleRight(), false);
/*                   if(leftLegAngle > RobotMap.LEG_OUT){
 *                       leftLegOut.whileHeld(new someothercommand);
 *                 }
 *                   if(rightLegAngle > RobotMap.LEG_OUT){
 *                       rightLegOut.whileHeld(new Shoot());
 *                   }
*/ 
                    // Calculate the leg angle in the YZ plane and use them to set the Leg Forward and Leg Back buttons */
                    leftLegYZ = AngleYZ(m_kinect.getSkeleton().GetHipLeft(), m_kinect.getSkeleton().GetAnkleLeft(), false);
                    rightLegYZ = AngleYZ(m_kinect.getSkeleton().GetHipRight(), m_kinect.getSkeleton().GetAnkleRight(), false);
                    if(rightLegYZ < RobotMap.LEG_FORWARD){
                        rightLegForward.whileHeld(new Shoot());
                    }
/*
 *                  if(rightLegYZ > RobotMap.LEG_BACKWARD){
 *                       rightLegBackward.whileHeld(new someothercommand);
 *                   }
 */
                    if(leftLegYZ < RobotMap.LEG_FORWARD){
                        //TODO - stop the SetKinectShotAngle Command
                        //       and the SetKinectShotSpeed Command
                        //       In short we need a way to change the Command
                        //       Mapping in the two Kinect Modes...
//                        leftLegForward.whenPressed(new DriveInKinect());
                    }
/*
 *                   if(leftLegYZ > RobotMap.LEG_BACKWARD){
 *                       leftLegBackward.whileHeld(new someothercommand);
 *                   }
 */
                }

    }
    
    /**
     * This method returns the angle (in degrees) of the vector pointing from Origin to Measured
     * projected to the XY plane. If the mirrored parameter is true the vector is flipped about the Y-axis.
     * Mirroring is used to avoid the region where the atan2 function is discontinuous
     * @param origin The Skeleton Joint to use as the origin point
     * @param measured The Skeleton Joint to use as the endpoint of the vector
     * @param mirrored Whether to mirror the X coordinate of the joint about the Y-axis
     * @return The angle in degrees
     */
    private double AngleXY(Skeleton.Joint origin, Skeleton.Joint measured, boolean mirrored){
        return Math.toDegrees(MathUtils.atan2(measured.getY()- origin.getY(),
                (mirrored) ? (origin.getX() - measured.getX()) : (measured.getX() - origin.getX())));
    }

     /**
     * This method returns the angle (in degrees) of the vector pointing from Origin to Measured
     * projected to the YZ plane. If the mirrored parameter is true the vector is flipped about the Y-axis.
     * Mirroring is used to avoid the region where the atan2 function is discontinuous
     * @param origin The Skeleton Joint to use as the origin point
     * @param measured The Skeleton Joint to use as the endpoint of the vector
     * @param mirrored Whether to mirror the Z coordinate of the joint about the Y-axis
     * @return The angle in degrees
     */
    private double AngleYZ(Skeleton.Joint origin, Skeleton.Joint measured, boolean mirrored){
        return Math.toDegrees(MathUtils.atan2(measured.getY()- origin.getY(),
                (mirrored) ? (origin.getZ() - measured.getZ()) : (measured.getZ() - origin.getZ())));
    }

    /**
     * This method checks if two Joints have z-coordinates within a given tolerance
     * @param origin
     * @param measured
     * @param tolerance
     * @return True if the z-coordinates are within tolerance
     */
    private boolean InSameZPlane(Skeleton.Joint origin, Skeleton.Joint measured, double tolerance)
        {
            return Math.abs(measured.getZ() - origin.getZ()) < tolerance;
        }

    /**
     * This method takes an input, an input range, and an output range,
     * and uses them to scale and constrain the input to the output range
     * @param input The input value to be manipulated
     * @param inputMin The minimum value of the input range
     * @param inputMax The maximum value of the input range
     * @param outputMin The minimum value of the output range
     * @param outputMax The maximum value of the output range
     * @return The output value scaled and constrained to the output range
     */
    private double CoerceToRange(double input, double inputMin, double inputMax, double outputMin, double outputMax)
        {
            /* Determine the center of the input range and output range */
            double inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            double outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            double scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            double result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            return Math.max(Math.min(result, outputMax), outputMin);
        }

        

}
