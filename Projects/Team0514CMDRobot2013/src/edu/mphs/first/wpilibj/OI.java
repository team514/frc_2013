
package edu.mphs.first.wpilibj;

//import edu.mphs.first.wpilibj.commands.BalanceOnBridge;
import edu.mphs.first.wpilibj.commands.Auto2PTShot;
import edu.mphs.first.wpilibj.commands.Auto3PTShot;
import edu.mphs.first.wpilibj.commands.BallMotorOff;
import edu.mphs.first.wpilibj.commands.BallMotorOn;
import edu.mphs.first.wpilibj.commands.DeployArm;
import edu.mphs.first.wpilibj.commands.DriveInArcade;
import edu.mphs.first.wpilibj.commands.DriveInTank;
import edu.mphs.first.wpilibj.commands.ManageShotAngle;
import edu.mphs.first.wpilibj.commands.ManageShotMotor;
import edu.mphs.first.wpilibj.commands.RetractArm;
import edu.mphs.first.wpilibj.commands.SetKinectShotSpeed;
import edu.mphs.first.wpilibj.commands.ShiftHigh;
import edu.mphs.first.wpilibj.commands.ShiftLow;
import edu.mphs.first.wpilibj.commands.Shoot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.InternalButton;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
        Joystick leftStick;
        Joystick rightStick;
        Joystick controller;
        JoystickButton highGear;
        JoystickButton lowGear;
        JoystickButton tank;
        JoystickButton arcade;
        JoystickButton ballOn;
        JoystickButton ballOff;
        JoystickButton armOut;
        JoystickButton armIn;
        JoystickButton shoot;
        JoystickButton angleup;
        JoystickButton angledown;
        JoystickButton shootslow;
        JoystickButton shootfast;
        JoystickButton balanceOn;
        JoystickButton balanceOff;
        JoystickButton shotscroll;
        JoystickButton anglescroll;
        
    
    public OI(){
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    leftStick = new Joystick(RobotMap.LEFT_JOYSTICK);
    rightStick = new Joystick(RobotMap.RIGHT_Z_JOYSTICK);
    controller = new Joystick(RobotMap.CONTROLLER);
    highGear = new JoystickButton(leftStick, RobotMap.Shift_High);
    lowGear = new JoystickButton(leftStick, RobotMap.Shift_Low);
    tank = new JoystickButton(rightStick, RobotMap.TANK_MODE);
    arcade = new JoystickButton(rightStick, RobotMap.ARCADE_MODE);
    ballOn = new JoystickButton(controller, RobotMap.BALL_ON);
    ballOff = new JoystickButton(controller, RobotMap.BALL_OFF);
    armOut = new JoystickButton(leftStick, RobotMap.ARMS_OUT);
    armIn = new JoystickButton(leftStick, RobotMap.ARMS_IN);
    shoot = new JoystickButton(controller, RobotMap.SHOOT);
    angleup = new JoystickButton(controller, RobotMap.SHOOT_UP);
    angledown = new JoystickButton(controller, RobotMap.SHOOT_DOWN);
    shootfast = new JoystickButton(controller, RobotMap.SHOOT_MORE);
    shootslow = new JoystickButton(controller, RobotMap.SHOOT_LESS);
    balanceOn = new JoystickButton(controller, RobotMap.Balance_On);
    balanceOff = new JoystickButton(controller, RobotMap.Balance_Off);
    shotscroll = new JoystickButton(controller, RobotMap.sScroll);
    anglescroll = new JoystickButton(controller, RobotMap.aScroll);
    
    SmartDashboard.putData(new Auto2PTShot());
    SmartDashboard.putData(new Auto3PTShot());
    SmartDashboard.putData(new SetKinectShotSpeed());
    
    highGear.whenPressed(new ShiftHigh());
    lowGear.whenPressed(new ShiftLow());
    
    tank.whenPressed(new DriveInTank());
    arcade.whenPressed(new DriveInArcade());
    
    ballOn.whenPressed(new BallMotorOn());
    ballOff.whenPressed(new BallMotorOff());
    
    armOut.whenPressed(new DeployArm());
    armIn.whenPressed(new RetractArm());
    
    shoot.whileHeld(new Shoot());
    
   angleup.whileHeld(new ManageShotAngle(true));
   angledown.whileHeld(new ManageShotAngle(false));

    shootslow.whenPressed(new ManageShotMotor(false));
    shootfast.whenPressed(new ManageShotMotor(true));

//    balanceOn.whenPressed(new BalanceOnBridge());
    
    }
    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);
  
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
    
    public Joystick getLeftstick(){
        return leftStick;
    }
    
    public Joystick getRightstick(){
        return rightStick;
    }
    
    public double getLeftDblY(){
        return leftStick.getY();
    }
    
    public double getRightDblY() {
        return rightStick.getY();
    }
    
    public double getLeftDblX(){
        return leftStick.getX();
    }
    
    public double getRightDblX(){
        return rightStick.getX();
    }
    
    public boolean getShotScroll(){
        return shotscroll.get();
    }
    
    public boolean getAngleScroll(){
        return anglescroll.get();
    }
        
}

