/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class DriveInTank extends CommandBase {
    
    public DriveInTank() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveUtil);
        
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        driveUtil.driveTank(oi.getLeftDblY(), oi.getRightDblY());
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        //TODO - Leave this as is.  Drive in Tank until the operator changes to Arcade.
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
