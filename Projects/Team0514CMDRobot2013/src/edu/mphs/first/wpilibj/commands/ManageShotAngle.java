/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class ManageShotAngle extends CommandBase {
    boolean direction, scroll, setangle;
    
    public ManageShotAngle(boolean direction) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotAngleUtil);
        this.direction = direction;
        this.scroll = false;
        setangle = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        this.scroll = oi.getAngleScroll();
        if(this.scroll){
            setScroll();
        }else{
            setToggle();
        }
    }
    
    private void setScroll(){
        double newposition;
        newposition = shotAngleUtil.getPosition();
        if(this.direction){
            newposition = (newposition - RobotMap.saConstant);
        }else{
            newposition = (newposition + RobotMap.saConstant);
        }
        shotAngleUtil.setPosition(newposition);
    }
    
    private void setToggle(){
        int newrange;
        newrange = shotAngleUtil.getRange();
        if(this.direction){
            newrange++;
        }else{
            newrange--;
        }
        shotAngleUtil.setRange(newrange);
        
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return shotAngleUtil.isOnTarget();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
