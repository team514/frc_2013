/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class Set3PTAngle extends CommandBase {
    boolean setpoint;
    
    public Set3PTAngle() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotAngleUtil);
        setpoint = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        shotAngleUtil.setPosition(RobotMap.SPC_3PT_Angle);
        this.setpoint = true;
        
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.setpoint;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
