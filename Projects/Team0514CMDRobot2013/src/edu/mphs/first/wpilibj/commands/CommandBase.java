package edu.mphs.first.wpilibj.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.mphs.first.wpilibj.OI;
import edu.mphs.first.wpilibj.OIK;
import edu.mphs.first.wpilibj.subsystems.BallGateUtil;
import edu.mphs.first.wpilibj.subsystems.BallUtil;
import edu.mphs.first.wpilibj.subsystems.BridgeUtil;
import edu.mphs.first.wpilibj.subsystems.CompressorUtil;
import edu.mphs.first.wpilibj.subsystems.DriveUtil;
//import edu.mphs.first.wpilibj.subsystems.ExampleSubsystem;
import edu.mphs.first.wpilibj.subsystems.ShiftUtil;
import edu.mphs.first.wpilibj.subsystems.ShotAngleUtil;
import edu.mphs.first.wpilibj.subsystems.ShotMotorUtil;
import edu.mphs.first.wpilibj.subsystems.ShotUtil;

/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command {

    public static OI oi;
    public static OIK oik;
    // Create a single static instance of all of your subsystems
//    public static ExampleSubsystem exampleSubsystem = new ExampleSubsystem();
    public static CompressorUtil compUtil = new CompressorUtil();
    public static DriveUtil driveUtil = new DriveUtil();
    public static ShiftUtil shiftUtil = new ShiftUtil();
    public static BallUtil ballUtil = new BallUtil();
    public static BridgeUtil bridgeUtil = new BridgeUtil();
    public static ShotUtil shotUtil = new ShotUtil();
    public static BallGateUtil ballGateUtil = new BallGateUtil();
    public static ShotMotorUtil shotMotorUtil = new ShotMotorUtil();
    public static ShotAngleUtil shotAngleUtil = new ShotAngleUtil();

    public static void init() {
        // This MUST be here. If the OI creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        oi = new OI();
        oik = new OIK();

        // Show what command your subsystem is running on the SmartDashboard
//        SmartDashboard.putData(exampleSubsystem);
        SmartDashboard.putData(compUtil);
        SmartDashboard.putData(driveUtil);
        SmartDashboard.putData(shiftUtil);
        SmartDashboard.putData(ballUtil);
        SmartDashboard.putData(bridgeUtil);
        SmartDashboard.putData(shotUtil);
        SmartDashboard.putData(ballGateUtil);
        SmartDashboard.putData(shotMotorUtil);
        SmartDashboard.putData(shotAngleUtil);
        
        
    }

    public CommandBase(String name) {
        super(name);
    }

    public CommandBase() {
        super();
    }
}
