/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class Set2PTShot extends CommandBase {
    boolean setspeed;
    public Set2PTShot() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotMotorUtil);
        this.setspeed = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        shotMotorUtil.setSpeed(RobotMap.Speed2PT);
        this.setspeed = true;
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.setspeed;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
