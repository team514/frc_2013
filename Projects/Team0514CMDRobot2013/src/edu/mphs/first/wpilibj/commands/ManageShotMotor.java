/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class ManageShotMotor extends CommandBase {
    boolean direction, scroll, setspeed;
    
    public ManageShotMotor(boolean direction) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotMotorUtil);
        this.direction = direction;
        this.scroll = false;
        this.setspeed = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        this.scroll = oi.getShotScroll();
        if(this.scroll){
            setScroll();
        }else{
            setToggle();
        }
    }
    
    private void setScroll(){
        double speed;
        speed = shotMotorUtil.getSpeed();
        if(this.direction){
            speed = (speed + RobotMap.ssConstant);
        }else{
            speed = (speed - RobotMap.ssConstant);
        }
        shotMotorUtil.setSpeed(speed);
        this.setspeed = true;
    }
    
    private void setToggle(){
        int newrange;
        newrange = shotMotorUtil.getRange();
        if(this.direction){
            newrange++;
        }else{
            newrange--;
        }
        shotMotorUtil.setRange(newrange);
        this.setspeed = true;        
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.setspeed;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
