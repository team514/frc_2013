/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class DriveInArcade extends CommandBase {
    
    public DriveInArcade() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        driveUtil.driveArcade(oi.getRightDblY(), oi.getRightDblX());
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        //Leave this as is.  Continue to drive in Arcade until it is changed to Tank by operator.
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
