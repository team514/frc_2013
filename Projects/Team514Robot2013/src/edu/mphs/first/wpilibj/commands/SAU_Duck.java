/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author Team 514
 */
public class SAU_Duck extends CommandBase {
    
    public SAU_Duck() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(angleUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(angleUtil.getPot() > (RobotMap.sau_DUCK - RobotMap.sau_WINDOW)){
            angleUtil.enableReverse();
        }
        if(angleUtil.getPot() < (RobotMap.sau_DUCK + RobotMap.sau_WINDOW)){
            angleUtil.enableForward();
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        boolean status = false;
        if(angleUtil.getUpper() || 
           angleUtil.getLower()){
            angleUtil.disableMotor();
            status = true;
        }
        if((angleUtil.getPot() > (RobotMap.sau_DUCK - RobotMap.sau_WINDOW)) && 
           (angleUtil.getPot() < (RobotMap.sau_DUCK + RobotMap.sau_WINDOW))){
            status = true;
        }
        return status;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
