/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class TU_FindTarget extends CommandBase {
    int myTarget;
    boolean done;
    
    public TU_FindTarget(int target) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(targetsUtil);
       // requires(driveUtil);
        requires(angleUtil);
        this.myTarget = target;
        this.done = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        targetsUtil.setTarget(myTarget);
       // driveUtil.driveTank(0.00, 0.00);
        angleUtil.disableMotor();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
       // if(this.myTarget != 3){
        //targetsUtil.manageTargets();
            if(targetsUtil.foundTarget()){
                angleUtil.disableMotor();
                this.done = true;
            }else{
                angleUtil.enableReverse();
            }
       // }else{
         //   this.done = true;
        }
    //}

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
//        oi.setTargetStatus(targetsUtil.onTargetY());
        return this.done;
        
    }

    // Called once after isFinished returns true
    protected void end() {
        //driveUtil.driveTank(0.00, 0.00);                
        angleUtil.disableMotor();
        
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    
}