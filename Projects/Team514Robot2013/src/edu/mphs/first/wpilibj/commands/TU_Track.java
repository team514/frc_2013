/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author Team 514
 */
public class TU_Track extends CommandBase {
    int myTarget;
    
    public TU_Track(int target) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(targetsUtil);
        requires(driveUtil);
        requires(angleUtil);
        this.myTarget = target;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        targetsUtil.setTarget(myTarget);
        driveUtil.driveTank(0.00, 0.00);
        angleUtil.disableMotor();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        targetsUtil.manageTargets();
            if(targetsUtil.foundTarget()){
                    followX(targetsUtil.getXDirection(),
                           targetsUtil.onTargetX());
                    followY(targetsUtil.getDistance(),
                            targetsUtil.getYDirection(),
                            targetsUtil.onTargetY());
            }else{
                driveUtil.driveTank(0.00, 0.00);
                angleUtil.disableMotor();
            }        
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
//        oi.setTargetStatus(targetsUtil.onTarget());
        return targetsUtil.onTarget();
    }

    // Called once after isFinished returns true
    protected void end() {
        driveUtil.driveTank(0.00, 0.00);                
        angleUtil.disableMotor();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    private void followY(double distance, boolean direction, boolean ontarget){
        
        if(ontarget){
            angleUtil.disableMotor();
        }else{
            if(direction){
                angleUtil.enableForward();
            }else{
                angleUtil.enableReverse();
            }
        }
    }
        
    private void followX(boolean direction, boolean ontarget){
        //double cmx;
        double leftX = RobotMap.tu_LEFT_MOTOR;
        double rightX = RobotMap.tu_RIGHT_MOTOR;
        
        //cmx = targetsUtil.getTargetCMX();
        
        //If object is directly in front, then use only distance to calculate speed.
        if(ontarget){
            leftX = 0.00;
            rightX = 0.00;
        }else{
            //Object is to the left or right.  Need to square inputs to left or right
            //motor depending on if you are in reverse and which side the target is...
            if(direction){
                rightX = -rightX;
            }else{
                leftX = -leftX;
            }
        }
        driveUtil.driveTank(leftX, rightX);
    }
        
    private double CoerceToRange(double inputMin, double inputMax,
                                 double outputMin, double outputMax,
                                 double input) {
        // TODO code application logic here
        double inputCenter;
        double outputCenter;
        double scale, result;
        double output;
        
            /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            output = Math.max(Math.min(result, outputMax), outputMin);
            return output;
                        
    }

}