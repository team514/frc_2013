package edu.mphs.first.wpilibj;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    // For example to map the left and right motors, you could define the
    // following variables to use with your drivetrain subsystem.
    // public static final int leftMotor = 1;
    // public static final int rightMotor = 2;
    
    // If you are using multiple modules, make sure to define both the port
    // number and the module. For example you with a rangefinder:
    // public static final int rangefinderPort = 1;
    // public static final int rangefinderModule = 1;
    // these variables map to digital sidecar
    public static final int LEFT_JAGUAR = 1;
    public static final int RIGHT_JAGUAR = 2;
    public static final int LEFT_STICK = 1;
    public static final int RIGHT_STICK = 2;
    public static final int controller = 3;
    public static final int yAxis = 4;
    public static final int xAxis = 5;
    
    
    // these variables map to the right joystick
    public static final int TANK_DRIVE = 2;
    public static final int ARCADE_DRIVE = 3;
    
    // these variables map to the controller
    public static final int yAxisPOS = 4;
    public static final int yAxisNEG = 2;
    public static final int xAxisNEG = 1;
    public static final int xAxisPOS = 3;
}
