
package edu.mphs.first.wpilibj;

//import edu.mphs.first.wpilibj.commands.CamTest;
import edu.mphs.first.wpilibj.commands.CamTest;
import edu.mphs.first.wpilibj.commands.DontFollowTarget;
import edu.mphs.first.wpilibj.commands.DriveInArcade;
import edu.mphs.first.wpilibj.commands.DriveInTank;
import edu.mphs.first.wpilibj.commands.FollowTarget;
import edu.mphs.first.wpilibj.commands.MoveX;
import edu.mphs.first.wpilibj.commands.MoveY;
import edu.mphs.first.wpilibj.commands.StopTrackImage;
import edu.mphs.first.wpilibj.commands.TrackImage;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);
    
    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);
    
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
    Joystick leftstick, rightstick, controller;
    JoystickButton tank,arcade;
    JoystickButton yAxisPOS, yAxisNEG, xAxisPOS, xAxisNEG;
    JoystickButton PIDTestLeft, PIDTestMid, PIDTestRight;
    JoystickButton TargetingOn, TargetingOff;
    
    public OI(){
        leftstick = new Joystick(RobotMap.LEFT_STICK);
        rightstick = new Joystick(RobotMap.RIGHT_STICK);
        controller = new Joystick(RobotMap.controller);
        
        tank = new JoystickButton(rightstick,RobotMap.TANK_DRIVE);
        arcade = new JoystickButton(rightstick,RobotMap.ARCADE_DRIVE);
        
        yAxisNEG = new JoystickButton(controller, RobotMap.yAxisNEG);
        yAxisPOS = new JoystickButton(controller, RobotMap.yAxisPOS);
        xAxisNEG = new JoystickButton(controller, RobotMap.xAxisNEG);
        xAxisPOS = new JoystickButton(controller, RobotMap.xAxisPOS);
        
        PIDTestLeft = new JoystickButton(controller, 5);  //Low
        PIDTestMid = new JoystickButton(controller, 6);   //Middle
        PIDTestRight = new JoystickButton(controller, 8); //High
        
        TargetingOff = new JoystickButton(controller, 9);
        TargetingOn = new JoystickButton(controller, 10);
        
        
        tank.whenPressed(new DriveInTank());
        arcade.whenPressed(new DriveInArcade());
        
        yAxisNEG.whileHeld(new MoveY(false));
        yAxisPOS.whileHeld(new MoveY(true));
        xAxisNEG.whileHeld(new MoveX(false));
        xAxisPOS.whileHeld(new MoveX(true));
        
//        PIDTestLeft.whenPressed(new CamTest(1));
//        PIDTestMid.whenPressed(new CamTest(2));
//        PIDTestRight.whenPressed(new CamTest(3));
        
        PIDTestLeft.whenPressed(new TrackImage(1));  //low
        PIDTestMid.whenPressed(new TrackImage(2));   //Middle
        PIDTestRight.whenPressed(new TrackImage(3)); //High

        TargetingOff.whenPressed(new DontFollowTarget());
        TargetingOn.whenPressed(new FollowTarget());
        
        
    }
    
    public double getrightY(){
        return rightstick.getY(GenericHID.Hand.kRight);
        
    }
    
    public double getleftY(){
        return leftstick.getY(GenericHID.Hand.kLeft);
    }
    
    public double getrightX(){
        return rightstick.getX(GenericHID.Hand.kRight);
    }
    
    public double getleftX(){
        return leftstick.getX(GenericHID.Hand.kLeft);
    }
    public Joystick getleft(){
        return leftstick;
    }
    public Joystick getright(){
        return rightstick;
    }
}

