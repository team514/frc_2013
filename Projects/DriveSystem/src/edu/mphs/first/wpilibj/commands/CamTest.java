/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class CamTest extends CommandBase {
    int target;
    
    public CamTest(int target) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(camXUtil);
        this.target = target;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        switch (this.target){
            case(1):
                //do something...
                camXUtil.setpoint(-.8, true);
                break;
            case(2):
                //do something...
                camXUtil.setpoint(0, true);
                break;
            case(3):
                //do sometthing...
                camXUtil.setpoint(.8, true);
                break;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return camXUtil.onTarget();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
