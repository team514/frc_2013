/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class TrackImage extends CommandBase {
    double setpointX, setpointY;
    int target;
    
    public TrackImage(int target) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(targetsUtil);
        requires(camXUtil);
        requires(camYUtil);
        setpointX = 0.0;
        setpointY = 0.0;
        this.target = target;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        targetsUtil.setTarget(this.target);
        this.setpointX = camXUtil.getpoint();
        this.setpointY = camYUtil.getpoint();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        targetsUtil.manageTargets();
            if(targetsUtil.foundTarget()){
                if(!targetsUtil.onTarget()){
                    //if(!camXUtil.onTarget()){
                        if(targetsUtil.getXDirection()){
                            this.setpointX = this.setpointX + .005;
                        }else{
                            this.setpointX = this.setpointX - .005;
                        }
                    camXUtil.setpoint(this.setpointX);
                    //}
                    //if(!camYUtil.onTarget()){
                        if(targetsUtil.getYDirection()){
                            this.setpointY = this.setpointY - .005;
                        }else{
                            this.setpointY = this.setpointY + .005;
                        }
                    camYUtil.setpoint(this.setpointY);
                    //}
                }
            }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished(){
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
