/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Team 514
 */
public class FollowTarget extends CommandBase {
    
    public FollowTarget() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(targetsUtil);
        requires(driveUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        targetsUtil.manageTargets();
            if(targetsUtil.foundTarget()){
                    follow(targetsUtil.getDistance(),
                           targetsUtil.getXDirection(),
                           targetsUtil.onTarget());
            }else{
                driveUtil.tank(0.0, 0.0);                
            }        
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
        
    private void follow(double distance, boolean direction, boolean straight){
        double cmx;
        double leftX = 0.00;
        double rightX = 0.00;
        boolean reverse = false;
        
        //If object is closer than 5 feet, back up!
        if(distance < 10){
            reverse = true;
            cmx = CoerceToRange(0, 10, 0.0, -0.50, distance);
        }else{
            cmx = CoerceToRange(10, 30, 0.5, 1.0, distance);            
        }
        
        //If object is directly in front, then use only distance to calculate speed.
        if(straight){
            leftX = cmx;
            rightX = cmx;
            
        }else{
            //Object is to the left or right.  Need to square inputs to left or right
            //motor depending on if you are in reverse and which side the target is...
            if(reverse){
                if(direction){
                    leftX = cmx*cmx;
                }else{
                    rightX = cmx*cmx;
                }
            }else{
                if(direction){
                    rightX = cmx*cmx;
                }else{
                    leftX = cmx*cmx;
                }
            }
        
        }
        
        SmartDashboard.putNumber("LeftX", leftX);
        SmartDashboard.putNumber("RightX", rightX);

        driveUtil.tank(leftX, -rightX);
   }
    
    private double CoerceToRange(double inputMin, double inputMax,
                                 double outputMin, double outputMax,
                                 double input) {
        // TODO code application logic here
        double inputCenter;
        double outputCenter;
        double scale, result;
        double output;
        
            /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            output = Math.max(Math.min(result, outputMax), outputMin);
            return output;
                        
    }

}