/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.command.PIDCommand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class PIDTest extends PIDCommand {
    PIDController pCON;
    int pointer;
    double output;
    
    public PIDTest(int pointer) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        super("PIDCommand1", 1.0, 0.0, 0.0, 0.02); 
        pCON = super.getPIDController();
        pCON.setInputRange(-1, 1);
        pCON.setOutputRange(0, 1);
//        requires(CommandBase.driveUtil);
        requires(CommandBase.camXUtil);
        this.pointer = pointer;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        pCON.enable();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        //need some logic here.
        switch (pointer){
            case(1): //Left Case
                pCON.setSetpoint(-.8);
                break;
            case(2): //Mid Case
                pCON.setSetpoint(0);
                break;
            case(3): //Right Case
                pCON.setSetpoint(.8);
                break;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        if((CommandBase.camXUtil.getpoint(true) >= (pCON.getSetpoint() - .01))  &&
           (CommandBase.camXUtil.getpoint(true) <= (pCON.getSetpoint() + .01))){
            return true;
        }else{
            return false;
        }
    }
    // Called once after isFinished returns true
    protected void end() {
        pCON.disable();
        updateStatus();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        pCON.disable();
    }

    protected double returnPIDInput() {
        //call a method to get the target values from the camera object
        //return CommandBase.visionUtil.getXspeed();
        return CommandBase.camXUtil.getpoint();
    }

    protected void usePIDOutput(double d) {
        //use the output from the PID to drive the motors
        CommandBase.camXUtil.setpoint(d);
        this.output = d;
    }
    
    private void updateStatus(){
        SmartDashboard.putNumber("PIDSource", CommandBase.camXUtil.getpoint());
        SmartDashboard.getNumber("PIDOutput", this.output);
        
    }
}
