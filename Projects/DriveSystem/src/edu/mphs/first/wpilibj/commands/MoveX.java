/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class MoveX extends CommandBase {
    double speed;
    boolean direction, setspeed;
    
    
    public MoveX(boolean direction) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(camXUtil);
        this.direction = direction;
        this.setspeed = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        this.speed = camXUtil.getpoint();
        if(this.direction){
            this.speed = this.speed + .005;
        }else{
            this.speed = this.speed - .005;
        }
        camXUtil.setpoint(this.speed);
        this.setspeed = true;
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.setspeed;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
