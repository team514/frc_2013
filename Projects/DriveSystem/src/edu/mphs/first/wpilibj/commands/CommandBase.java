package edu.mphs.first.wpilibj.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.mphs.first.wpilibj.OI;
import edu.mphs.first.wpilibj.subsystems.DriveUtil;
import edu.mphs.first.wpilibj.subsystems.CamXUtil;
import edu.mphs.first.wpilibj.subsystems.CamYUtil;
import edu.mphs.first.wpilibj.subsystems.MagazineUtil;
import edu.mphs.first.wpilibj.subsystems.TargetsUtil;


/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command {

    public static OI oi;
    // Create a single static instance of all of your subsystems
    public static DriveUtil driveUtil = new DriveUtil();
    public static CamXUtil camXUtil = new CamXUtil();
    public static CamYUtil camYUtil = new CamYUtil();
    public static TargetsUtil targetsUtil = new TargetsUtil();
    public static MagazineUtil magUtil = new MagazineUtil();

    public static void init() {
        // This MUST be here. If the OI creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        oi = new OI();

        // Show what command your subsystem is running on the SmartDashboard
        SmartDashboard.putData(driveUtil);
        SmartDashboard.putData(camXUtil);
        SmartDashboard.putData(camYUtil);
        SmartDashboard.putData(targetsUtil);
        SmartDashboard.putData(magUtil);
    }

    public CommandBase(String name) {
        super(name);
    }

    public CommandBase() {
        super();
    }
}
