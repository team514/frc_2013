/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.DriveInTank;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Team 514
 */
public class DriveUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    RobotDrive drive;
    boolean drivemode;
    SpeedController leftmotor, rightmotor;
    double stick1X, stick1Y, stick2Y;
    
    public DriveUtil() {
        leftmotor = new Victor(RobotMap.LEFT_JAGUAR);
        rightmotor = new Victor(RobotMap.RIGHT_JAGUAR);
        drive = new RobotDrive(leftmotor, rightmotor);
        //drive = new RobotDrive(2, 9);
        drive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
        drive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        //drive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, false);
        //drive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, false);
    }
    public void arcade(double stick1X, double stick1Y){
        leftmotor.set(stick1X);
        rightmotor.set(stick1Y);
        this.stick1Y = stick1Y;
        this.stick1X = stick1X;
        this.stick2Y = 0.0;
        
    }
    public void arcade(Joystick stick){
                drive.arcadeDrive(stick, false);
    }
    public void tank(double stick1Y, double stick2Y){
        leftmotor.set(stick1Y);
        rightmotor.set(stick2Y);
        this.stick1Y = stick1Y;
        this.stick2Y = stick2Y;
        this.stick1X = 0.0;
        
    }
    public void tank(Joystick lstick, Joystick rstick){
        drive.tankDrive(lstick, rstick);
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new DriveInTank());
    }
    public void updateStatus(){
        //SmartDashboard.putNumber("DriveUtil stick1X",this.stick1X);
        //SmartDashboard.putNumber("DriveUtil stick1Y", this.stick1Y);
        //SmartDashboard.putNumber("DriveUtil stick2Y", this.stick2Y);
    }
}
