/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Team 514
 */
public class CamYUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    Servo yServo;
    double target;
    
    public CamYUtil(){
        yServo = new Servo(RobotMap.yAxis);
        
        //xServo.setBounds(1, .01, .5, .01, 0);
        //yServo.setBounds(max, deadbandMax, center, deadbandMin, min);
        this.target = 0;
        setpoint(.5);
    }
    
    public void setpoint(double value){
        //value = CoerceToRange(-1.0, 1.0, 0.0, 1.0, value);
        yServo.set(checkrange(value));
    }

    public void setpoint(double value, boolean coerce){
            value = CoerceToRange(-1, 1, 0.0, 1.0, value);
            this.target = value;
            yServo.set(value);
    }
    
    public double getpoint(){
        //return CoerceToRange(0.0, 1.0, -1.0, 1.0, yServo.get);
        return yServo.get();
    }

    public double getpoint(boolean coerce){
        return CoerceToRange(0.0, 1.0, -1.0, 1.0, yServo.get());
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        //setDefaultCommand(new StopServos());
        
    }
    
    private double CoerceToRange(double inputMin, double inputMax,
                                 double outputMin, double outputMax,
                                 double input) {
        // TODO code application logic here
        double inputCenter;
        double outputCenter;
        double scale, result;
        double output;
        
            /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            output = Math.max(Math.min(result, outputMax), outputMin);
            return output;
            
    }
    
    private double checkrange(double value){
        if(value < 0.0){
            value = 0.0;
        }
        if(value > 1.0){
            value = 1.0;
        }
        return value;
    }
    
    public void updateStatus(){
        SmartDashboard.putNumber("Servo Y get()", yServo.get());
        SmartDashboard.putNumber("Servo Y getAngle()", yServo.getAngle());
        SmartDashboard.putBoolean("Servo Y On Target", this.onTarget());
    }
    
    public boolean onTarget(){
        //TODO - Check the difference between this ranging factor and
        //the getpoint(true) - .01 or some othe value to se the window!
        if((this.target >= (getpoint() - .1)) &&
           (this.target <= (getpoint() + .1))){
            return true;
        }else{
            return false;
        }
    }
}