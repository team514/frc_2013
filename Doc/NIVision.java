{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fcharset0 Arial;}}
{\*\generator Msftedit 5.41.15.1515;}\viewkind4\uc1\pard\f0\fs20 /*----------------------------------------------------------------------------*/\par
/* Copyright (c) FIRST 2008-2012. All Rights Reserved.                        */\par
/* Open Source Software - may be modified and shared by FRC teams. The code   */\par
/* must be accompanied by the FIRST BSD license file in the root directory of */\par
/* the project.                                                               */\par
/*----------------------------------------------------------------------------*/\par
package edu.wpi.first.wpilibj.image;\par
\par
import com.sun.cldc.jna.BlockingFunction;\par
import com.sun.cldc.jna.Function;\par
import com.sun.cldc.jna.NativeLibrary;\par
import com.sun.cldc.jna.Pointer;\par
import com.sun.cldc.jna.Structure;\par
import com.sun.cldc.jna.TaskExecutor;\par
import com.sun.cldc.jna.ptr.IntByReference;\par
import edu.wpi.first.wpilibj.util.BoundaryException;\par
\par
/**\par
 * Class for interfacing with the NIVision libraries\par
 * @author dtjones\par
 */\par
public class NIVision \{\par
\par
    private NIVision() \{\par
    \}\par
\par
    private static final TaskExecutor taskExecutor = new TaskExecutor("nivision task");\par
\par
    /**\par
     * Enumeration representing the possible types of imaq images\par
     */\par
    public static class ImageType \{\par
\par
        public final int value;\par
        /** The image type is 8-bit unsigned integer grayscale. */\par
        public static final ImageType imaqImageU8 = new ImageType(0);\par
        /** The image type is 16-bit unsigned integer grayscale. */\par
        public static final ImageType imaqImageU16 = new ImageType(7);\par
        /** The image type is 16-bit signed integer grayscale. */\par
        public static final ImageType imaqImageI16 = new ImageType(1);\par
        /** The image type is 32-bit floating-point grayscale. */\par
        public static final ImageType imaqImageSGL = new ImageType(2);\par
        /** The image type is complex. */\par
        public static final ImageType imaqImageComplex = new ImageType(3);\par
        /** The image type is RGB color. */\par
        public static final ImageType imaqImageRGB = new ImageType(4);\par
        /** The image type is HSL color. */\par
        public static final ImageType imaqImageHSL = new ImageType(5);\par
        /** The image type is 64-bit unsigned RGB color. */\par
        public static final ImageType imaqImageRGBU64 = new ImageType(6);\par
        /** Reserved */\par
        public static final ImageType imaqImageTypeSizeGuard = new ImageType(0xFFFFFFFF);\par
\par
        private ImageType(int value) \{\par
            this.value = value;\par
        \}\par
    \}\par
\par
    /**\par
     * Enumerations representing the possible color spaces to operate in\par
     */\par
    public static class ColorMode \{\par
\par
        public final int value;\par
        /** The function operates in the RGB (Red, Blue, Green) color space. */\par
        public static final ColorMode IMAQ_RGB = new ColorMode(0);\par
        /** The function operates in the HSL (Hue, Saturation, Luminance) color space. */\par
        public static final ColorMode IMAQ_HSL = new ColorMode(1);\par
        /** The function operates in the HSV (Hue, Saturation, Value) color space. */\par
        public static final ColorMode IMAQ_HSV = new ColorMode(2);\par
        /** The function operates in the HSI (Hue, Saturation, Intensity) color space. */\par
        public static final ColorMode IMAQ_HSI = new ColorMode(3);\par
        /** The function operates in the CIE L*a*b* color space. */\par
        public static final ColorMode IMAQ_CIE = new ColorMode(4);\par
        /** The function operates in the CIE XYZ color space. */\par
        public static final ColorMode IMAQ_CIEXYZ = new ColorMode(5);\par
        public static final ColorMode IMAQ_COLOR_MODE_SIZE_GUARD = new ColorMode(0xFFFFFFFF);\par
\par
        private ColorMode(int value) \{\par
            this.value = value;\par
        \}\par
    \}\par
\par
    public static class MeasurementType \{\par
\par
        public final int value;\par
        /** X-coordinate of the point representing the average position of the total particle mass, assuming every point in the particle has a constant density. */\par
        public static final MeasurementType IMAQ_MT_CENTER_OF_MASS_X = new MeasurementType(0);\par
        /** Y-coordinate of the point representing the average position of the total particle mass, assuming every point in the particle has a constant density. */\par
        public static final MeasurementType IMAQ_MT_CENTER_OF_MASS_Y = new MeasurementType(1);\par
        /** X-coordinate of the highest, leftmost particle pixel. */\par
        public static final MeasurementType IMAQ_MT_FIRST_PIXEL_X = new MeasurementType(2);\par
        /** Y-coordinate of the highest, leftmost particle pixel. */\par
        public static final MeasurementType IMAQ_MT_FIRST_PIXEL_Y = new MeasurementType(3);\par
        /** X-coordinate of the leftmost particle point. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_LEFT = new MeasurementType(4);\par
        /** Y-coordinate of highest particle point. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_TOP = new MeasurementType(5);\par
        /** X-coordinate of the rightmost particle point. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_RIGHT = new MeasurementType(6);\par
        /** Y-coordinate of the lowest particle point. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_BOTTOM = new MeasurementType(7);\par
        /** X-coordinate of the start of the line segment connecting the two perimeter points that are the furthest apart. */\par
        public static final MeasurementType IMAQ_MT_MAX_FERET_DIAMETER_START_X = new MeasurementType(8);\par
        /** Y-coordinate of the start of the line segment connecting the two perimeter points that are the furthest apart. */\par
        public static final MeasurementType IMAQ_MT_MAX_FERET_DIAMETER_START_Y = new MeasurementType(9);\par
        /** X-coordinate of the end of the line segment connecting the two perimeter points that are the furthest apart. */\par
        public static final MeasurementType IMAQ_MT_MAX_FERET_DIAMETER_END_X = new MeasurementType(10);\par
        /** Y-coordinate of the end of the line segment connecting the two perimeter points that are the furthest apart. */\par
        public static final MeasurementType IMAQ_MT_MAX_FERET_DIAMETER_END_Y = new MeasurementType(11);\par
        /** X-coordinate of the leftmost pixel in the longest row of contiguous pixels in the particle. */\par
        public static final MeasurementType IMAQ_MT_MAX_HORIZ_SEGMENT_LENGTH_LEFT = new MeasurementType(12);\par
        /** X-coordinate of the rightmost pixel in the longest row of contiguous pixels in the particle. */\par
        public static final MeasurementType IMAQ_MT_MAX_HORIZ_SEGMENT_LENGTH_RIGHT = new MeasurementType(13);\par
        /** Y-coordinate of all of the pixels in the longest row of contiguous pixels in the particle. */\par
        public static final MeasurementType IMAQ_MT_MAX_HORIZ_SEGMENT_LENGTH_ROW = new MeasurementType(14);\par
        /** Distance between the x-coordinate of the leftmost particle point and the x-coordinate of the rightmost particle point. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_WIDTH = new MeasurementType(16);\par
        /** Distance between the y-coordinate of highest particle point and the y-coordinate of the lowest particle point. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_HEIGHT = new MeasurementType(17);\par
        /** Distance between opposite corners of the bounding rectangle. */\par
        public static final MeasurementType IMAQ_MT_BOUNDING_RECT_DIAGONAL = new MeasurementType(18);\par
        /** Length of the outer boundary of the particle. */\par
        public static final MeasurementType IMAQ_MT_PERIMETER = new MeasurementType(19);\par
        /** Perimeter of the smallest convex polygon containing all points in the particle. */\par
        public static final MeasurementType IMAQ_MT_CONVEX_HULL_PERIMETER = new MeasurementType(20);\par
        /** Sum of the perimeters of each hole in the particle. */\par
        public static final MeasurementType IMAQ_MT_HOLES_PERIMETER = new MeasurementType(21);\par
        /** Distance between the start and end of the line segment connecting the two perimeter points that are the furthest apart. */\par
        public static final MeasurementType IMAQ_MT_MAX_FERET_DIAMETER = new MeasurementType(22);\par
        /** Length of the major axis of the ellipse with the same perimeter and area as the particle. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_ELLIPSE_MAJOR_AXIS = new MeasurementType(23);\par
        /** Length of the minor axis of the ellipse with the same perimeter and area as the particle. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_ELLIPSE_MINOR_AXIS = new MeasurementType(24);\par
        /** Length of the minor axis of the ellipse with the same area as the particle, and Major Axis equal in length to the Max Feret Diameter. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_ELLIPSE_MINOR_AXIS_FERET = new MeasurementType(25);\par
        /** Longest side of the rectangle with the same perimeter and area as the particle. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_RECT_LONG_SIDE = new MeasurementType(26);\par
        /** Shortest side of the rectangle with the same perimeter and area as the particle. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_RECT_SHORT_SIDE = new MeasurementType(27);\par
        /** Distance between opposite corners of the rectangle with the same perimeter and area as the particle. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_RECT_DIAGONAL = new MeasurementType(28);\par
        /** Shortest side of the rectangle with the same area as the particle, and longest side equal in length to the Max Feret Diameter. */\par
        public static final MeasurementType IMAQ_MT_EQUIVALENT_RECT_SHORT_SIDE_FERET = new MeasurementType(29);\par
        /** Average length of a horizontal segment in the particle. */\par
        public static final MeasurementType IMAQ_MT_AVERAGE_HORIZ_SEGMENT_LENGTH = new MeasurementType(30);\par
        /** Average length of a vertical segment in the particle. */\par
        public static final MeasurementType IMAQ_MT_AVERAGE_VERT_SEGMENT_LENGTH = new MeasurementType(31);\par
        /** The particle area divided by the particle perimeter. */\par
        public static final MeasurementType IMAQ_MT_HYDRAULIC_RADIUS = new MeasurementType(32);\par
        /** Diameter of a disk with the same area as the particle. */\par
        public static final MeasurementType IMAQ_MT_WADDEL_DISK_DIAMETER = new MeasurementType(33);\par
        /** Area of the particle. */\par
        public static final MeasurementType IMAQ_MT_AREA = new MeasurementType(35);\par
        /** Sum of the areas of each hole in the particle. */\par
        public static final MeasurementType IMAQ_MT_HOLES_AREA = new MeasurementType(36);\par
        /** Area of a particle that completely covers the image. */\par
        public static final MeasurementType IMAQ_MT_PARTICLE_AND_HOLES_AREA = new MeasurementType(37);\par
        /** Area of the smallest convex polygon containing all points in the particle. */\par
        public static final MeasurementType IMAQ_MT_CONVEX_HULL_AREA = new MeasurementType(38);\par
        /** Area of the image. */\par
        public static final MeasurementType IMAQ_MT_IMAGE_AREA = new MeasurementType(39);\par
        /** Number of holes in the particle. */\par
        public static final MeasurementType IMAQ_MT_NUMBER_OF_HOLES = new MeasurementType(41);\par
        /** Number of horizontal segments in the particle. */\par
        public static final MeasurementType IMAQ_MT_NUMBER_OF_HORIZ_SEGMENTS = new MeasurementType(42);\par
        /** Number of vertical segments in the particle. */\par
        public static final MeasurementType IMAQ_MT_NUMBER_OF_VERT_SEGMENTS = new MeasurementType(43);\par
        /** The angle of the line that passes through the particle Center of Mass about which the particle has the lowest moment of inertia. */\par
        public static final MeasurementType IMAQ_MT_ORIENTATION = new MeasurementType(45);\par
        /** The angle of the line segment connecting the two perimeter points that are the furthest apart. */\par
        public static final MeasurementType IMAQ_MT_MAX_FERET_DIAMETER_ORIENTATION = new MeasurementType(46);\par
        /** Percentage of the particle Area covering the Image Area. */\par
        public static final MeasurementType IMAQ_MT_AREA_BY_IMAGE_AREA = new MeasurementType(48);\par
        /** Percentage of the particle Area in relation to its Particle and Holes Area. */\par
        public static final MeasurementType IMAQ_MT_AREA_BY_PARTICLE_AND_HOLES_AREA = new MeasurementType(49);\par
        /** Equivalent Ellipse Major Axis divided by Equivalent Ellipse Minor Axis. */\par
        public static final MeasurementType IMAQ_MT_RATIO_OF_EQUIVALENT_ELLIPSE_AXES = new MeasurementType(50);\par
        /** Equivalent Rect Long Side divided by Equivalent Rect Short Side. */\par
        public static final MeasurementType IMAQ_MT_RATIO_OF_EQUIVALENT_RECT_SIDES = new MeasurementType(51);\par
        /** Max Feret Diameter divided by Equivalent Rect Short Side (Feret). */\par
        public static final MeasurementType IMAQ_MT_ELONGATION_FACTOR = new MeasurementType(53);\par
        /** Area divided by the product of Bounding Rect Width and Bounding Rect Height. */\par
        public static final MeasurementType IMAQ_MT_COMPACTNESS_FACTOR = new MeasurementType(54);\par
        /** Perimeter divided by the circumference of a circle with the same area. */\par
        public static final MeasurementType IMAQ_MT_HEYWOOD_CIRCULARITY_FACTOR = new MeasurementType(55);\par
        /** Factor relating area to moment of inertia. */\par
        public static final MeasurementType IMAQ_MT_TYPE_FACTOR = new MeasurementType(56);\par
        /** The sum of all x-coordinates in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_X = new MeasurementType(58);\par
        /** The sum of all y-coordinates in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_Y = new MeasurementType(59);\par
        /** The sum of all x-coordinates squared in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_XX = new MeasurementType(60);\par
        /** The sum of all x-coordinates times y-coordinates in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_XY = new MeasurementType(61);\par
        /** The sum of all y-coordinates squared in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_YY = new MeasurementType(62);\par
        /** The sum of all x-coordinates cubed in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_XXX = new MeasurementType(63);\par
        /** The sum of all x-coordinates squared times y-coordinates in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_XXY = new MeasurementType(64);\par
        /** The sum of all x-coordinates times y-coordinates squared in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_XYY = new MeasurementType(65);\par
        /** The sum of all y-coordinates cubed in the particle. */\par
        public static final MeasurementType IMAQ_MT_SUM_YYY = new MeasurementType(66);\par
        /** The moment of inertia in the x-direction twice. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_XX = new MeasurementType(68);\par
        /** The moment of inertia in the x and y directions. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_XY = new MeasurementType(69);\par
        /** The moment of inertia in the y-direction twice. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_YY = new MeasurementType(70);\par
        /** The moment of inertia in the x-direction three times. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_XXX = new MeasurementType(71);\par
        /** The moment of inertia in the x-direction twice and the y-direction once. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_XXY = new MeasurementType(72);\par
        /** The moment of inertia in the x-direction once and the y-direction twice. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_XYY = new MeasurementType(73);\par
        /** The moment of inertia in the y-direction three times. */\par
        public static final MeasurementType IMAQ_MT_MOMENT_OF_INERTIA_YYY = new MeasurementType(74);\par
        /** The normalized moment of inertia in the x-direction twice. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_XX = new MeasurementType(75);\par
        /** The normalized moment of inertia in the x- and y-directions. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_XY = new MeasurementType(76);\par
        /** The normalized moment of inertia in the y-direction twice. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_YY = new MeasurementType(77);\par
        /** The normalized moment of inertia in the x-direction three times. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_XXX = new MeasurementType(78);\par
        /** The normalized moment of inertia in the x-direction twice and the y-direction once. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_XXY = new MeasurementType(79);\par
        /** The normalized moment of inertia in the x-direction once and the y-direction twice. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_XYY = new MeasurementType(80);\par
        /** The normalized moment of inertia in the y-direction three times. */\par
        public static final MeasurementType IMAQ_MT_NORM_MOMENT_OF_INERTIA_YYY = new MeasurementType(81);\par
        /** The first Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_1 = new MeasurementType(82);\par
        /** The second Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_2 = new MeasurementType(83);\par
        /** The third Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_3 = new MeasurementType(84);\par
        /** The fourth Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_4 = new MeasurementType(85);\par
        /** The fifth Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_5 = new MeasurementType(86);\par
        /** The sixth Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_6 = new MeasurementType(87);\par
        /** The seventh Hu moment. */\par
        public static final MeasurementType IMAQ_MT_HU_MOMENT_7 = new MeasurementType(88);\par
\par
        public static final MeasurementType IMAQ_MEASUREMENT_TYPE_SIZE_GUARD = new MeasurementType(0xFFFFFFFF);\par
\par
        private MeasurementType(int value) \{\par
            this.value = value;\par
        \}\par
    \}\par
\par
    public static class Range extends Structure \{\par
\par
        int lower;\par
        int upper;\par
\par
        public void read() \{\par
            lower = backingNativeMemory.getInt(0);\par
            upper = backingNativeMemory.getInt(4);\par
        \}\par
\par
        public void write() \{\par
            backingNativeMemory.setInt(0, lower);\par
            backingNativeMemory.setInt(4, upper);\par
        \}\par
\par
        public int size() \{\par
            return 8;\par
        \}\par
\par
        /**\par
         * Free the memory used by this range\par
         */\par
        public void free() \{\par
            release();\par
        \}\par
\par
        /**\par
         * Create a new range with the specified upper and lower boundaries\par
         * @param lower The lower limit\par
         * @param upper The upper limit\par
         */\par
        public Range(final int lower, final int upper) \{\par
            allocateMemory();\par
            set(lower, upper);\par
        \}\par
\par
        /**\par
         * Set the upper and lower boundaries\par
         * @param lower The lower limit\par
         * @param upper The upper limit\par
         */\par
        public void set(final int lower, final int upper) \{\par
            if (lower > upper) \{\par
                throw new BoundaryException("Lower boundary is greater than upper");\par
            \}\par
            this.lower = lower;\par
            this.upper = upper;\par
            write();\par
        \}\par
\par
        /**\par
         * Get the lower boundary\par
         * @return The lower boundary.\par
         */\par
        public int getLower() \{\par
            read();\par
            return lower;\par
        \}\par
\par
        /**\par
         * Get the upper boundary\par
         * @return The upper boundary.\par
         */\par
        public int getUpper() \{\par
            read();\par
            return upper;\par
        \}\par
    \}\par
\par
    //============================================================================\par
    //  Value Type handling\par
    // Parameters of "type" PixelValue, RGBValue, HSLValue, etc, are passed as an encoded int from Java code.\par
    //============================================================================\par
    public int encodeRGBValue(int r, int g, int b, int a) \{\par
        return (a & 0xff << 24) | (r & 0xff << 16) | (g & 0xff << 8) | (b & 0xff);\par
    \}\par
\par
    public int rgbGetR(int rgbvalue) \{\par
        return (rgbvalue >>> 16) & 0xFF;\par
    \}\par
\par
    public int rgbGetG(int rgbvalue) \{\par
        return (rgbvalue >>> 8) & 0xFF;\par
    \}\par
\par
    public int rgbGetB(int rgbvalue) \{\par
        return (rgbvalue >>> 0) & 0xFF;\par
    \}\par
\par
    public int rgbGetA(int rgbvalue) \{\par
        return (rgbvalue >>> 24) & 0xFF;\par
    \}\par
\par
    public int encodeHSLValue(int h, int s, int l, int a) \{\par
        return (a & 0xff << 24) | (h & 0xff << 16) | (s & 0xff << 8) | (l & 0xff);\par
    \}\par
\par
    public int hslGetH(int hslvalue) \{\par
        return (hslvalue >>> 16) & 0xFF;\par
    \}\par
\par
    public int hslGetS(int hslvalue) \{\par
        return (hslvalue >>> 8) & 0xFF;\par
    \}\par
\par
    public int hslGetL(int hslvalue) \{\par
        return (hslvalue >>> 0) & 0xFF;\par
    \}\par
\par
    public int hslGetA(int hslvalue) \{\par
        return (hslvalue >>> 24) & 0xFF;\par
    \}\par
\par
    public int encodeHSVValue(int h, int s, int v, int a) \{\par
        return (a & 0xff << 24) | (h & 0xff << 16) | (s & 0xff << 8) | (v & 0xff);\par
    \}\par
\par
    public int hsvGetH(int hsvvalue) \{\par
        return (hsvvalue >>> 16) & 0xFF;\par
    \}\par
\par
    public int hsvGetS(int hsvvalue) \{\par
        return (hsvvalue >>> 8) & 0xFF;\par
    \}\par
\par
    public int hsvGetV(int hsvvalue) \{\par
        return (hsvvalue >>> 0) & 0xFF;\par
    \}\par
\par
    public int hsvGetA(int hsvvalue) \{\par
        return (hsvvalue >>> 24) & 0xFF;\par
    \}\par
\par
    public int encodeGreyscaleValue(float greyscale) \{\par
        return Float.floatToIntBits(greyscale);\par
    \}\par
\par
    public float decodeGreyscaleValue(int pixelValue) \{\par
        return Float.intBitsToFloat(pixelValue);\par
    \}\par
\par
    //============================================================================\par
    //  Acquisition functions\par
    //============================================================================\par
    //IMAQ_FUNC Image* IMAQ_STDCALL imaqCopyFromRing(SESSION_ID sessionID, Image* image, int imageToCopy, int* imageNumber, Rect rect);\par
    //IMAQ_FUNC Image* IMAQ_STDCALL imaqEasyAcquire(const char* interfaceName);\par
    //IMAQ_FUNC Image* IMAQ_STDCALL imaqExtractFromRing(SESSION_ID sessionID, int imageToExtract, int* imageNumber);\par
    //IMAQ_FUNC Image* IMAQ_STDCALL imaqGrab(SESSION_ID sessionID, Image* image, int immediate);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqReleaseImage(SESSION_ID sessionID);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetupGrab(SESSION_ID sessionID, Rect rect);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetupRing(SESSION_ID sessionID, Image** images, int numImages, int skipCount, Rect rect);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetupSequence(SESSION_ID sessionID, Image** images, int numImages, int skipCount, Rect rect);\par
    //IMAQ_FUNC Image* IMAQ_STDCALL imaqSnap(SESSION_ID sessionID, Image* image, Rect rect);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqStartAcquisition(SESSION_ID sessionID);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqStopAcquisition(SESSION_ID sessionID);\par
\par
    //============================================================================\par
    //  Arithmetic functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAbsoluteDifference(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAbsoluteDifferenceConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAdd(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAddConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAverage(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAverageConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqDivide2(Image* dest, const Image* sourceA, const Image* sourceB, RoundingMode roundingMode);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqDivideConstant2(Image* dest, const Image* source, PixelValue value, RoundingMode roundingMode);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMax(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMaxConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMin(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMinConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqModulo(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqModuloConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMulDiv(Image* dest, const Image* sourceA, const Image* sourceB, float value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMultiply(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMultiplyConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqSubtract(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqSubtractConstant(Image* dest, const Image* source, PixelValue value);\par
\par
    //============================================================================\par
    //  Spatial Filters functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqCannyEdgeFilter(Image* dest, const Image* source, const CannyOptions* options);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqConvolve2(Image* dest, Image* source, float* kernel, int matrixRows, int matrixCols, float normalize, Image* mask, RoundingMode roundingMode);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqCorrelate(Image* dest, Image* source, const Image* templateImage, Rect rect);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqEdgeFilter(Image* dest, Image* source, OutlineMethod method, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqLowPass(Image* dest, Image* source, int width, int height, float tolerance, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMedianFilter(Image* dest, Image* source, int width, int height, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqNthOrderFilter(Image* dest, Image* source, int width, int height, int n, const Image* mask);\par
\par
    //============================================================================\par
    //  Drawing functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqDrawLineOnImage(Image* dest, const Image* source, DrawMode mode, Point start, Point end, float newPixelValue);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqDrawShapeOnImage(Image* dest, const Image* source, Rect rect, DrawMode mode, ShapeMode shape, float newPixelValue);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqDrawTextOnImage(Image* dest, const Image* source, Point coord, const char* text, const DrawTextOptions* options, int* fontNameUsed);\par
\par
    //============================================================================\par
    //  Interlacing functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqInterlaceCombine(Image* frame, const Image* odd, const Image* even);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqInterlaceSeparate(const Image* frame, Image* odd, Image* even);\par
\par
    //============================================================================\par
    //  Image Information functions\par
    //============================================================================\par
    //IMAQ_FUNC char** IMAQ_STDCALL imaqEnumerateCustomKeys(const Image* image, unsigned int* size);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetBitDepth(const Image* image, unsigned int* bitDepth);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetBytesPerPixel(const Image* image, int* byteCount);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetImageInfo(const Image* image, ImageInfo* info);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetImageType(const Image* image, ImageType* type);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetMaskOffset(const Image* image, Point* offset);\par
    //IMAQ_FUNC void*  IMAQ_STDCALL imaqGetPixelAddress(const Image* image, Point pixel);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetVisionInfoTypes(const Image* image, unsigned int* present);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqIsImageEmpty(const Image* image, int* empty);\par
    //IMAQ_FUNC void*  IMAQ_STDCALL imaqReadCustomData(const Image* image, const char* key, unsigned int* size);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqRemoveCustomData(Image* image, const char* key);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqRemoveVisionInfo2(const Image* image, unsigned int info);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetBitDepth(Image* image, unsigned int bitDepth);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetImageSize(Image* image, int width, int height);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetMaskOffset(Image* image, Point offset);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqWriteCustomData(Image* image, const char* key, const void* data, unsigned int size);\par
\par
    // TODO: Make non-blocking...\par
    private static final BlockingFunction imaqGetImageSizeFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqGetImageSize");\par
    static \{ imaqGetImageSizeFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Get the height of an image.\par
     * @param image The image to get the height of.\par
     * @return The height of the image.\par
     */\par
    public static int getHeight(Pointer image)  throws NIVisionException\{\par
        Pointer i = new Pointer(4);\par
        int val;\par
        try \{\par
            assertCleanStatus(imaqGetImageSizeFn.call3(image, 0, i));\par
        \} finally \{\par
            val = i.getInt(0);\par
            i.free();\par
        \}\par
        return val;\par
    \}\par
\par
    /**\par
     * Get the width of an image.\par
     * @param image The image to get the width of.\par
     * @return The width of the image.\par
     */\par
    public static int getWidth(Pointer image)  throws NIVisionException\{\par
        Pointer i = new Pointer(4);\par
        int val;\par
        try \{\par
            assertCleanStatus(imaqGetImageSizeFn.call3(image, i, 0));\par
        \} finally \{\par
            val = i.getInt(0);\par
            i.free();\par
        \}\par
        return val;\par
    \}\par
\par
    //============================================================================\par
    //  Morphology functions\par
    //============================================================================\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqConvexHull(Image* dest, Image* source, int connectivity8);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqDanielssonDistance(Image* dest, Image* source);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqFillHoles(Image* dest, const Image* source, int connectivity8);\par
    //IMAQ_FUNC CircleReport* IMAQ_STDCALL imaqFindCircles(Image* dest, Image* source, float minRadius, float maxRadius, int* numCircles);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqLabel2(Image* dest, Image* source, int connectivity8, int* particleCount);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqMorphology(Image* dest, Image* source, MorphologyMethod method, const StructuringElement* structuringElement);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqRejectBorder(Image* dest, Image* source, int connectivity8);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqSegmentation(Image* dest, Image* source);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqSeparation(Image* dest, Image* source, int erosions, const StructuringElement* structuringElement);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqSimpleDistance(Image* dest, Image* source, const StructuringElement* structuringElement);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqSizeFilter(Image* dest, Image* source, int connectivity8, int erosions, SizeType keepSize, const StructuringElement* structuringElement);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqSkeleton(Image* dest, Image* source, SkeletonMethod method);\par
\par
    //============================================================================\par
    //  Logical functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAnd(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAndConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqCompare(Image* dest, const Image* source, const Image* compareImage, ComparisonFunction compare);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqCompareConstant(Image* dest, const Image* source, PixelValue value, ComparisonFunction compare);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqLogicalDifference(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqLogicalDifferenceConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqNand(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqNandConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqNor(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqNorConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOr(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOrConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqXnor(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqXnorConstant(Image* dest, const Image* source, PixelValue value);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqXor(Image* dest, const Image* sourceA, const Image* sourceB);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqXorConstant(Image* dest, const Image* source, PixelValue value);\par
\par
    //============================================================================\par
    //  Particle Analysis functions\par
    //============================================================================\par
\par
    private static final BlockingFunction imaqCountParticlesFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqCountParticles");\par
    static \{ imaqCountParticlesFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Counts the number of particles in a binary image.\par
     * @param image The image to count the particles in\par
     * @return The number of particles\par
     */\par
    public static int countParticles(Pointer image)  throws NIVisionException\{\par
        IntByReference i = new IntByReference(0);\par
        int val;\par
        try \{\par
            assertCleanStatus(imaqCountParticlesFn.call3(image, 1, i.getPointer()));\par
        \} finally \{\par
            val = i.getValue();\par
            i.free();\par
        \}\par
        return val;\par
    \}\par
\par
    private static final BlockingFunction imaqMeasureParticleFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqMeasureParticle");\par
    static \{ imaqMeasureParticleFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Returns a measurement associated with a particle\par
     * @param image The image containing the particle to get information about.\par
     * @param particleNum The number of the particle to get information about.\par
     * @param calibrated Specifies whether to return the measurement as a real-world value.\par
     * @param type The measurement to make on the particle.\par
     * @return The value of the requested measurement.\par
     */\par
    public static double MeasureParticle (Pointer image, int particleNum, boolean calibrated, MeasurementType type)  throws NIVisionException\{\par
        Pointer l = new Pointer(8);\par
        double val;\par
        try \{\par
            assertCleanStatus(imaqMeasureParticleFn.call5(image, particleNum, calibrated?1:0, type.value, l));\par
        \} finally \{\par
            val = l.getDouble(0);\par
            l.free();\par
        \}\par
        return val;\par
    \}\par
\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqParticleFilter4(Image* dest, Image* source, const ParticleFilterCriteria2* criteria, int criteriaCount, const ParticleFilterOptions2* options, const ROI* roi, int* numParticles);\par
\par
    //============================================================================\par
    //  Analytic Geometry functions\par
    //============================================================================\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqBuildCoordinateSystem(const Point* points, ReferenceMode mode, AxisOrientation orientation, CoordinateSystem* system);\par
    //IMAQ_FUNC BestCircle2*  IMAQ_STDCALL imaqFitCircle2(const PointFloat* points, int numPoints, const FitCircleOptions* options);\par
    //IMAQ_FUNC BestEllipse2* IMAQ_STDCALL imaqFitEllipse2(const PointFloat* points, int numPoints, const FitEllipseOptions* options);\par
    //IMAQ_FUNC BestLine*     IMAQ_STDCALL imaqFitLine(const PointFloat* points, int numPoints, const FitLineOptions* options);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetAngle(PointFloat start1, PointFloat end1, PointFloat start2, PointFloat end2, float* angle);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetBisectingLine(PointFloat start1, PointFloat end1, PointFloat start2, PointFloat end2, PointFloat* bisectStart, PointFloat* bisectEnd);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetDistance(PointFloat point1, PointFloat point2, float* distance);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetIntersection(PointFloat start1, PointFloat end1, PointFloat start2, PointFloat end2, PointFloat* intersection);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetMidLine(PointFloat refLineStart, PointFloat refLineEnd, PointFloat point, PointFloat* midLineStart, PointFloat* midLineEnd);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetPerpendicularLine(PointFloat refLineStart, PointFloat refLineEnd, PointFloat point, PointFloat* perpLineStart, PointFloat* perpLineEnd, double* distance);\par
    //IMAQ_FUNC SegmentInfo*  IMAQ_STDCALL imaqGetPointsOnContour(const Image* image, int* numSegments);\par
    //IMAQ_FUNC Point*        IMAQ_STDCALL imaqGetPointsOnLine(Point start, Point end, int* numPoints);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetPolygonArea(const PointFloat* points, int numPoints, float* area);\par
    //IMAQ_FUNC float*        IMAQ_STDCALL imaqInterpolatePoints(const Image* image, const Point* points, int numPoints, InterpolationMethod method, int subpixel, int* interpCount);\par
\par
    //============================================================================\par
    //  Border functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqFillBorder(Image* image, BorderMethod method);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqGetBorderSize(const Image* image, int* borderSize);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqSetBorderSize(Image* image, int size);\par
\par
    //============================================================================\par
    //  Image Management functions\par
    //============================================================================\par
    protected static final BlockingFunction imaqCreateImageFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqCreateImage");\par
    static \{ imaqCreateImageFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Allocates space for and creates a new imaq image\par
     * @param type the imaq encoding to use for the image\par
     * @param borderSize the size of border to use for the image\par
     * @return a newly allocated pointer to an imaqImage\par
     */\par
    public static Pointer imaqCreateImage(ImageType type, int borderSize) throws NIVisionException\{\par
        int toReturn = imaqCreateImageFn.call2(type.value, borderSize);\par
        assertCleanStatus(toReturn);\par
        return new Pointer(toReturn, 0);\par
    \}\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqArrayToImage(Image* image, const void* array, int numCols, int numRows);\par
    //IMAQ_FUNC void*  IMAQ_STDCALL imaqImageToArray(const Image* image, Rect rect, int* columns, int* rows);\par
\par
    //============================================================================\par
    //  Color Processing functions\par
    //============================================================================\par
\par
    private static final BlockingFunction imaqColorThresholdFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqColorThreshold");\par
    static \{ imaqColorThresholdFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Convert the given image into a binary image true where the colors match the given thresholds\par
     * @param dest the 8 bit monocolor image to store the result in\par
     * @param source the color image source\par
     * @param mode The mode of color to use\par
     * @param plane1Range First color range\par
     * @param plane2Range Second color range\par
     * @param plane3Range Third color range\par
     */\par
    public static void colorThreshold(Pointer dest, Pointer source, ColorMode mode,\par
            Pointer plane1Range, Pointer plane2Range, Pointer plane3Range)  throws NIVisionException\{\par
        int replaceValue = 1;\par
        assertCleanStatus(imaqColorThresholdFn.call7(dest.address().toUWord().toPrimitive(),\par
                source.address().toUWord().toPrimitive(),\par
                replaceValue, mode.value,\par
                plane1Range.address().toUWord().toPrimitive(),\par
                plane2Range.address().toUWord().toPrimitive(),\par
                plane3Range.address().toUWord().toPrimitive()));\par
    \}\par
\par
    private static final BlockingFunction imaqColorEqualizeFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqColorEqualize");\par
    static \{ imaqColorEqualizeFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Calculates the histogram of each plane of a color image and redistributes\par
     * pixel values across the desired range while maintaining pixel value\par
     * groupings.\par
     * @param destination The destination image.\par
     * @param source The image to equalize.\par
     * @param all Set this parameter to TRUE to equalize all three planes of the\par
     * image. Set this parameter to FALSE to equalize only the luminance plane.\par
     */\par
    public static void colorEqualize(Pointer destination, Pointer source, boolean all)  throws NIVisionException\{\par
        assertCleanStatus(imaqColorEqualizeFn.call3(destination, source,all?1:0));\par
    \}\par
\par
    //IMAQ_FUNC Color2                IMAQ_STDCALL imaqChangeColorSpace2(const Color2* sourceColor, ColorMode sourceSpace, ColorMode destSpace, double offset, const CIEXYZValue* whiteReference);\par
    //IMAQ_FUNC int                   IMAQ_STDCALL imaqColorBCGTransform(Image* dest, const Image* source, const BCGOptions* redOptions, const BCGOptions* greenOptions, const BCGOptions* blueOptions, const Image* mask);\par
    //IMAQ_FUNC ColorHistogramReport* IMAQ_STDCALL imaqColorHistogram2(Image* image, int numClasses, ColorMode mode, const CIEXYZValue* whiteReference, Image* mask);\par
    //IMAQ_FUNC int                   IMAQ_STDCALL imaqColorLookup(Image* dest, const Image* source, ColorMode mode, const Image* mask, const short* plane1, const short* plane2, const short* plane3);\par
\par
    //============================================================================\par
    //  Transform functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqBCGTransform(Image* dest, const Image* source, const BCGOptions* options, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqEqualize(Image* dest, const Image* source, float min, float max, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqInverse(Image* dest, const Image* source, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqLookup(Image* dest, const Image* source, const short* table, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMathTransform(Image* dest, const Image* source, MathTransformMethod method, float rangeMin, float rangeMax, float power, const Image* mask);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqWatershedTransform(Image* dest, const Image* source, int connectivity8, int* zoneCount);\par
\par
\par
    //============================================================================\par
    //  Utilities functions\par
    //============================================================================\par
    //IMAQ_FUNC const float* IMAQ_STDCALL imaqGetKernel(KernelFamily family, int size, int number);\par
    //IMAQ_FUNC Annulus      IMAQ_STDCALL imaqMakeAnnulus(Point center, int innerRadius, int outerRadius, double startAngle, double endAngle);\par
    //IMAQ_FUNC Point        IMAQ_STDCALL imaqMakePoint(int xCoordinate, int yCoordinate);\par
    //IMAQ_FUNC PointFloat   IMAQ_STDCALL imaqMakePointFloat(float xCoordinate, float yCoordinate);\par
    //IMAQ_FUNC Rect         IMAQ_STDCALL imaqMakeRect(int top, int left, int height, int width);\par
    //IMAQ_FUNC Rect         IMAQ_STDCALL imaqMakeRectFromRotatedRect(RotatedRect rotatedRect);\par
    //IMAQ_FUNC RotatedRect  IMAQ_STDCALL imaqMakeRotatedRect(int top, int left, int height, int width, double angle);\par
    //IMAQ_FUNC RotatedRect  IMAQ_STDCALL imaqMakeRotatedRectFromRect(Rect rect);\par
    //IMAQ_FUNC int          IMAQ_STDCALL imaqMulticoreOptions(MulticoreOperation operation, unsigned int* customNumCores);\par
\par
    //============================================================================\par
    //  Image Manipulation functions\par
    //============================================================================\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqCast(Image* dest, const Image* source, ImageType type, const float* lookup, int shift);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqCopyRect(Image* dest, const Image* source, Rect rect, Point destLoc);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqDuplicate(Image* dest, const Image* source);\par
    //IMAQ_FUNC void* IMAQ_STDCALL imaqFlatten(const Image* image, FlattenType type, CompressionType compression, int quality, unsigned int* size);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqFlip(Image* dest, const Image* source, FlipAxis axis);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqMask(Image* dest, const Image* source, const Image* mask);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqResample(Image* dest, const Image* source, int newWidth, int newHeight, InterpolationMethod method, Rect rect);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqRotate2(Image* dest, const Image* source, float angle, PixelValue fill, InterpolationMethod method, int maintainSize);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqScale(Image* dest, const Image* source, int xScale, int yScale, ScalingMode scaleMode, Rect rect);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqShift(Image* dest, const Image* source, int shiftX, int shiftY, PixelValue fill);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqTranspose(Image* dest, const Image* source);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqUnflatten(Image* image, const void* data, unsigned int size);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqUnwrapImage(Image* dest, const Image* source, Annulus annulus, RectOrientation orientation, InterpolationMethod method);\par
    //IMAQ_FUNC int   IMAQ_STDCALL imaqView3D(Image* dest, Image* source, const View3DOptions* options);\par
\par
    //============================================================================\par
    //  Pattern Matching functions\par
    //============================================================================\par
\par
    private static final BlockingFunction imaqDetectEllipsesFn =\par
            NativeLibrary.getDefaultInstance().getBlockingFunction("imaqDetectEllipses");\par
    static \{ imaqDetectEllipsesFn.setTaskExecutor(NIVision.taskExecutor); \}\par
    private static Pointer numberOfEllipsesDetected = new Pointer(4);\par
\par
\par
    public static EllipseMatch[] detectEllipses(MonoImage image, EllipseDescriptor ellipseDescriptor,\par
            CurveOptions curveOptions, ShapeDetectionOptions shapeDetectionOptions,\par
            RegionOfInterest roi) throws NIVisionException \{\par
\par
        int curveOptionsPointer = 0;\par
        if (curveOptions != null)\par
            curveOptionsPointer = curveOptions.getPointer().address().toUWord().toPrimitive();\par
        int shapeDetectionOptionsPointer = 0;\par
        if (shapeDetectionOptions != null)\par
            shapeDetectionOptionsPointer = shapeDetectionOptions.getPointer().address().toUWord().toPrimitive();\par
        int roiPointer = 0;\par
        if (roi != null)\par
            roiPointer = roi.getPointer().address().toUWord().toPrimitive();\par
\par
        int returnedAddress =\par
                imaqDetectEllipsesFn.call6(\par
                image.image.address().toUWord().toPrimitive(),\par
                ellipseDescriptor.getPointer().address().toUWord().toPrimitive(),\par
                curveOptionsPointer, shapeDetectionOptionsPointer,\par
                roiPointer,\par
                numberOfEllipsesDetected.address().toUWord().toPrimitive());\par
\par
        try \{\par
            NIVision.assertCleanStatus(returnedAddress);\par
        \} catch (NIVisionException ex) \{\par
            if (!ex.getMessage().equals("No error."))\par
                throw ex;\par
        \}\par
\par
        EllipseMatch[] matches = EllipseMatch.getMatchesFromMemory(returnedAddress, numberOfEllipsesDetected.getInt(0));\par
        NIVision.dispose(new Pointer(returnedAddress,0));\par
        return matches;\par
    \}\par
    //IMAQ_FUNC CircleMatch*              IMAQ_STDCALL imaqDetectCircles(const Image* image, const CircleDescriptor* circleDescriptor, const CurveOptions* curveOptions, const ShapeDetectionOptions* shapeDetectionOptions, const ROI* roi, int* numMatchesReturned);\par
    //IMAQ_FUNC LineMatch*                IMAQ_STDCALL imaqDetectLines(const Image* image, const LineDescriptor* lineDescriptor, const CurveOptions* curveOptions, const ShapeDetectionOptions* shapeDetectionOptions, const ROI* roi, int* numMatchesReturned);\par
    //IMAQ_FUNC RectangleMatch*           IMAQ_STDCALL imaqDetectRectangles(const Image* image, const RectangleDescriptor* rectangleDescriptor, const CurveOptions* curveOptions, const ShapeDetectionOptions* shapeDetectionOptions, const ROI* roi, int* numMatchesReturned);\par
    //IMAQ_FUNC FeatureData*              IMAQ_STDCALL imaqGetGeometricFeaturesFromCurves(const Curve* curves, unsigned int numCurves, const FeatureType* featureTypes, unsigned int numFeatureTypes, unsigned int* numFeatures);\par
    //IMAQ_FUNC FeatureData*              IMAQ_STDCALL imaqGetGeometricTemplateFeatureInfo(const Image* pattern, unsigned int* numFeatures);\par
    //IMAQ_FUNC int                       IMAQ_STDCALL imaqLearnColorPattern(Image* image, const LearnColorPatternOptions* options);\par
    //IMAQ_FUNC int                       IMAQ_STDCALL imaqLearnGeometricPattern(Image* image, PointFloat originOffset, const CurveOptions* curveOptions, const LearnGeometricPatternAdvancedOptions* advancedLearnOptions, const Image* mask);\par
    //IMAQ_FUNC MultipleGeometricPattern* IMAQ_STDCALL imaqLearnMultipleGeometricPatterns(const Image** patterns, unsigned int numberOfPatterns, const String255* labels);\par
    //IMAQ_FUNC int                       IMAQ_STDCALL imaqLearnPattern3(Image* image, LearningMode learningMode, LearnPatternAdvancedOptions* advancedOptions, const Image* mask);\par
    //IMAQ_FUNC PatternMatch*             IMAQ_STDCALL imaqMatchColorPattern(const Image* image, Image* pattern, const MatchColorPatternOptions* options, Rect searchRect, int* numMatches);\par
    //IMAQ_FUNC GeometricPatternMatch2*   IMAQ_STDCALL imaqMatchGeometricPattern2(const Image* image, const Image* pattern, const CurveOptions* curveOptions, const MatchGeometricPatternOptions* matchOptions, const MatchGeometricPatternAdvancedOptions2* advancedMatchOptions, const ROI* roi, int* numMatches);\par
    //IMAQ_FUNC GeometricPatternMatch2*   IMAQ_STDCALL imaqMatchMultipleGeometricPatterns(const Image* image, const MultipleGeometricPattern* multiplePattern, const ROI* roi, int* numMatches);\par
    //IMAQ_FUNC PatternMatch*             IMAQ_STDCALL imaqMatchPattern2(const Image* image, const Image* pattern, const MatchPatternOptions* options, const MatchPatternOptions* advancedOptions, Rect searchRect, int* numMatches);\par
    //IMAQ_FUNC MultipleGeometricPattern* IMAQ_STDCALL imaqReadMultipleGeometricPatternFile(const char* fileName, String255 description);\par
    //IMAQ_FUNC PatternMatch*             IMAQ_STDCALL imaqRefineMatches(const Image* image, const Image* pattern, const PatternMatch* candidatesIn, int numCandidatesIn, MatchPatternOptions* options, MatchPatternAdvancedOptions* advancedOptions, int* numCandidatesOut);\par
    //IMAQ_FUNC int                       IMAQ_STDCALL imaqSetMultipleGeometricPatternsOptions(MultipleGeometricPattern* multiplePattern, const char* label, const CurveOptions* curveOptions, const MatchGeometricPatternOptions* matchOptions, const MatchGeometricPatternAdvancedOptions2* advancedMatchOptions);\par
    //IMAQ_FUNC int                       IMAQ_STDCALL imaqWriteMultipleGeometricPatternFile(const MultipleGeometricPattern* multiplePattern, const char* fileName, const char* description);\par
\par
    //============================================================================\par
    //  Calibration functions\par
    //============================================================================\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqCopyCalibrationInfo2(Image* dest, Image* source, Point offset);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqCorrectCalibratedImage(Image* dest, const Image* source, PixelValue fill, InterpolationMethod method, const ROI* roi);\par
    //IMAQ_FUNC CalibrationInfo* IMAQ_STDCALL imaqGetCalibrationInfo2(const Image* image);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqLearnCalibrationGrid(Image* image, const ROI* roi, const LearnCalibrationOptions* options, const GridDescriptor* grid, const CoordinateSystem* system, const RangeFloat* range, float* quality);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqLearnCalibrationPoints(Image* image, const CalibrationPoints* points, const ROI* roi, const LearnCalibrationOptions* options, const GridDescriptor* grid, const CoordinateSystem* system, float* quality);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqSetCoordinateSystem(Image* image, const CoordinateSystem* system);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqSetSimpleCalibration(Image* image, ScalingMethod method, int learnTable, const GridDescriptor* grid, const CoordinateSystem* system);\par
    //IMAQ_FUNC TransformReport* IMAQ_STDCALL imaqTransformPixelToRealWorld(const Image* image, const PointFloat* pixelCoordinates, int numCoordinates);\par
    //IMAQ_FUNC TransformReport* IMAQ_STDCALL imaqTransformRealWorldToPixel(const Image* image, const PointFloat* realWorldCoordinates, int numCoordinates);\par
\par
    //============================================================================\par
    //  Overlay functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqClearOverlay(Image* image, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqCopyOverlay(Image* dest, const Image* source, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqGetOverlayProperties(Image* image, const char* group, TransformBehaviors* transformBehaviors);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqMergeOverlay(Image* dest, const Image* source, const RGBValue* palette, unsigned int numColors, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayArc(Image* image, const ArcInfo* arc, const RGBValue* color, DrawMode drawMode, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayBitmap(Image* image, Point destLoc, const RGBValue* bitmap, unsigned int numCols, unsigned int numRows, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayClosedContour(Image* image, const Point* points, int numPoints, const RGBValue* color, DrawMode drawMode, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayLine(Image* image, Point start, Point end, const RGBValue* color, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayMetafile(Image* image, const void* metafile, Rect rect, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayOpenContour(Image* image, const Point* points, int numPoints, const RGBValue* color, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayOval(Image* image, Rect boundingBox, const RGBValue* color, DrawMode drawMode, char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayPoints(Image* image, const Point* points, int numPoints, const RGBValue* colors, int numColors, PointSymbol symbol, const UserPointSymbol* userSymbol, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayRect(Image* image, Rect rect, const RGBValue* color, DrawMode drawMode, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayROI(Image* image, const ROI* roi, PointSymbol symbol, const UserPointSymbol* userSymbol, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqOverlayText(Image* image, Point origin, const char* text, const RGBValue* color, const OverlayTextOptions* options, const char* group);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqSetOverlayProperties(Image* image, const char* group, TransformBehaviors* transformBehaviors);\par
\par
    //============================================================================\par
    //  Color Matching functions\par
    //============================================================================\par
    //IMAQ_FUNC ColorInformation* IMAQ_STDCALL imaqLearnColor(const Image* image, const ROI* roi, ColorSensitivity sensitivity, int saturation);\par
    //IMAQ_FUNC int*              IMAQ_STDCALL imaqMatchColor(const Image* image, const ColorInformation* info, const ROI* roi, int* numScores);\par
\par
    //============================================================================\par
    //  Meter functions\par
    //============================================================================\par
    //IMAQ_FUNC MeterArc* IMAQ_STDCALL imaqGetMeterArc(int lightNeedle, MeterArcMode mode, const ROI* roi, PointFloat base, PointFloat start, PointFloat end);\par
    //IMAQ_FUNC int       IMAQ_STDCALL imaqReadMeter(const Image* image, const MeterArc* arcInfo, double* percentage, PointFloat* endOfNeedle);\par
\par
    //============================================================================\par
    //  Barcode I/O functions\par
    //============================================================================\par
    //IMAQ_FUNC int               IMAQ_STDCALL imaqGradeDataMatrixBarcodeAIM(const Image* image, AIMGradeReport* report);\par
    //IMAQ_FUNC BarcodeInfo*      IMAQ_STDCALL imaqReadBarcode(const Image* image, BarcodeType type, const ROI* roi, int validate);\par
    //IMAQ_FUNC DataMatrixReport* IMAQ_STDCALL imaqReadDataMatrixBarcode2(Image* image, const ROI* roi, DataMatrixGradingMode prepareForGrading, const DataMatrixDescriptionOptions* descriptionOptions, const DataMatrixSizeOptions* sizeOptions, const DataMatrixSearchOptions* searchOptions);\par
    //IMAQ_FUNC Barcode2DInfo*    IMAQ_STDCALL imaqReadPDF417Barcode(const Image* image, const ROI* roi, Barcode2DSearchMode searchMode, unsigned int* numBarcodes);\par
    //IMAQ_FUNC QRCodeReport*     IMAQ_STDCALL imaqReadQRCode(Image* image, const ROI* roi, QRGradingMode reserved, const QRCodeDescriptionOptions* descriptionOptions, const QRCodeSizeOptions* sizeOptions, const QRCodeSearchOptions* searchOptions);\par
\par
    //============================================================================\par
    //  Shape Matching functions\par
    //============================================================================\par
    //IMAQ_FUNC ShapeReport* IMAQ_STDCALL imaqMatchShape(Image* dest, Image* source, const Image* templateImage, int scaleInvariant, int connectivity8, double tolerance, int* numMatches);\par
\par
    //============================================================================\par
    //  Contours functions\par
    //============================================================================\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddAnnulusContour(ROI* roi, Annulus annulus);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddClosedContour(ROI* roi, const Point* points, int numPoints);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddLineContour(ROI* roi, Point start, Point end);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddOpenContour(ROI* roi, const Point* points, int numPoints);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddOvalContour(ROI* roi, Rect boundingBox);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddPointContour(ROI* roi, Point point);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddRectContour(ROI* roi, Rect rect);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqAddRotatedRectContour2(ROI* roi, RotatedRect rect);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqCopyContour(ROI* destRoi, const ROI* sourceRoi, ContourID id);\par
    //IMAQ_FUNC ContourID     IMAQ_STDCALL imaqGetContour(const ROI* roi, unsigned int index);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetContourColor(const ROI* roi, ContourID id, RGBValue* contourColor);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqGetContourCount(const ROI* roi);\par
    //IMAQ_FUNC ContourInfo2* IMAQ_STDCALL imaqGetContourInfo2(const ROI* roi, ContourID id);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqMoveContour(ROI* roi, ContourID id, int deltaX, int deltaY);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqRemoveContour(ROI* roi, ContourID id);\par
    //IMAQ_FUNC int           IMAQ_STDCALL imaqSetContourColor(ROI* roi, ContourID id, const RGBValue* color);\par
\par
    //============================================================================\par
    //  Regions of Interest functions\par
    //============================================================================\par
    //IMAQ_FUNC int  IMAQ_STDCALL imaqConstructROI2(const Image* image, ROI* roi, Tool initialTool, const ToolWindowOptions* tools, const ConstructROIOptions2* options, int* okay);\par
    //IMAQ_FUNC ROI* IMAQ_STDCALL imaqCreateROI();\par
    //IMAQ_FUNC int  IMAQ_STDCALL imaqGetROIBoundingBox(const ROI* roi, Rect* boundingBox);\par
    //IMAQ_FUNC int  IMAQ_STDCALL imaqGetROIColor(const ROI* roi, RGBValue* roiColor);\par
    //IMAQ_FUNC ROI* IMAQ_STDCALL imaqGetWindowROI(int windowNumber);\par
    //IMAQ_FUNC int  IMAQ_STDCALL imaqSetROIColor(ROI* roi, const RGBValue* color);\par
    //IMAQ_FUNC int  IMAQ_STDCALL imaqSetWindowROI(int windowNumber, const ROI* roi);\par
\par
    //============================================================================\par
    //  Image Analysis functions\par
    //============================================================================\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqCentroid(const Image* image, PointFloat* centroid, const Image* mask);\par
    //IMAQ_FUNC Curve*           IMAQ_STDCALL imaqExtractCurves(const Image* image, const ROI* roi, const CurveOptions* curveOptions, unsigned int* numCurves);\par
    //IMAQ_FUNC HistogramReport* IMAQ_STDCALL imaqHistogram(const Image* image, int numClasses, float min, float max, const Image* mask);\par
    //IMAQ_FUNC LinearAverages*  IMAQ_STDCALL imaqLinearAverages2(Image* image, LinearAveragesMode mode, Rect rect);\par
    //IMAQ_FUNC LineProfile*     IMAQ_STDCALL imaqLineProfile(const Image* image, Point start, Point end);\par
    //IMAQ_FUNC QuantifyReport*  IMAQ_STDCALL imaqQuantify(const Image* image, const Image* mask);\par
\par
    //============================================================================\par
    //  Error Management functions\par
    //============================================================================\par
    private static final BlockingFunction imaqGetLastErrorFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqGetLastError");\par
    static \{ imaqGetLastErrorFn.setTaskExecutor(taskExecutor);  \}\par
\par
    public static int getLastError()\{\par
        return imaqGetLastErrorFn.call0();\par
    \}\par
    protected static void assertCleanStatus (int code) throws NIVisionException \{\par
        if (code == 0) \{\par
            throw new NIVisionException(imaqGetLastErrorFn.call0());\par
        \}\par
    \}\par
\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqClearError();\par
    //IMAQ_FUNC char*       IMAQ_STDCALL imaqGetErrorText(int errorCode);\par
    //IMAQ_FUNC const char* IMAQ_STDCALL imaqGetLastErrorFunc();\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqSetError(int errorCode, const char* function);\par
\par
    //============================================================================\par
    //  Threshold functions\par
    //============================================================================\par
    //IMAQ_FUNC ThresholdData* IMAQ_STDCALL imaqAutoThreshold2(Image* dest, const Image* source, int numClasses, ThresholdMethod method, const Image* mask);\par
    //IMAQ_FUNC int            IMAQ_STDCALL imaqLocalThreshold(Image* dest, const Image* source, unsigned int windowWidth, unsigned int windowHeight, LocalThresholdMethod method, double deviationWeight, ObjectType type, float replaceValue);\par
    //IMAQ_FUNC int            IMAQ_STDCALL imaqMagicWand(Image* dest, const Image* source, Point coord, float tolerance, int connectivity8, float replaceValue);\par
    //IMAQ_FUNC int            IMAQ_STDCALL imaqMultithreshold(Image* dest, const Image* source, const ThresholdData* ranges, int numRanges);\par
    //IMAQ_FUNC int            IMAQ_STDCALL imaqThreshold(Image* dest, const Image* source, float rangeMin, float rangeMax, int useNewValue, float newValue);\par
\par
    //============================================================================\par
    //  Memory Management functions\par
    //============================================================================\par
\par
    private static final BlockingFunction imaqDisposeFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqDispose");\par
    static \{ imaqDisposeFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Cleans up resources associated with images, regions of interest (ROIs),\par
     * arrays, and reports that you no longer need.\par
     * After you dispose of something, you can no longer use it.\par
     * @param item The image, ROI, array, or report whose memory you want to free.\par
     */\par
    public static void dispose (Pointer item)  throws NIVisionException\{\par
        assertCleanStatus(imaqDisposeFn.call1(item));\par
    \}\par
\par
    //============================================================================\par
    //  File I/O functions\par
    //============================================================================\par
\par
    private static final BlockingFunction Priv_ReadJPEGString_CFn = NativeLibrary.getDefaultInstance().getBlockingFunction("Priv_ReadJPEGString_C");\par
    static \{ Priv_ReadJPEGString_CFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Read a pointer to a byte array into the given image\par
     * @param image pointer to an imaq image object\par
     * @param p pointer to a byte array holding image data\par
     */\par
    public static void readJpegString(Pointer image, Pointer p) \{\par
        Priv_ReadJPEGString_CFn.call3(image, p, p.getSize());\par
    \}\par
\par
    private static final BlockingFunction Priv_SetWriteFileAllowedFn = NativeLibrary.getDefaultInstance().getBlockingFunction("Priv_SetWriteFileAllowed");\par
    static \{ Priv_SetWriteFileAllowedFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Set true to be able to create files on cRio\par
     * @param val true allows files to be created\par
     */\par
    public static void setWriteFileAllowed(boolean val) \{\par
        Priv_SetWriteFileAllowedFn.call1(val ? 1 : 0);\par
    \}\par
\par
    private static final Function imaqWriteFileFn = NativeLibrary.getDefaultInstance().getFunction("imaqWriteFile");\par
//    static \{ imaqWriteFileFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Write an image to the given file.\par
     *\par
     * Supported extensions:\par
     * .aipd or .apd AIPD\par
     * .bmp BMP\par
     * .jpg or .jpeg JPEG\par
     * .jp2 JPEG2000\par
     * .png PNG\par
     * .tif or .tiff TIFF\par
     *\par
     * @param image The image to write to a file.\par
     * @param fileName The name of the destination file.\par
     */\par
    public static void writeFile(Pointer image, String fileName) throws NIVisionException \{\par
        Pointer p = new Pointer(fileName.length() + 1);\par
        p.setString(0, fileName);\par
        setWriteFileAllowed(true);\par
        try \{\par
            assertCleanStatus(imaqWriteFileFn.call3(image, p, 0)); //zero is unused color table\par
        \} finally \{\par
            p.free();\par
        \}\par
    \}\par
\par
    /**\par
     * Write an image to the given file.\par
     *\par
     * Supported extensions:\par
     * .aipd or .apd AIPD\par
     * .bmp BMP\par
     * .jpg or .jpeg JPEG\par
     * .jp2 JPEG2000\par
     * .png PNG\par
     * .tif or .tiff TIFF\par
     *\par
     * @param image The image to write to a file.\par
     * @param fileName The name of the destination file.\par
     * @param colorTable The color table to use for 8-bit images\par
     */\par
    public static void writeFile(Pointer image, String fileName, Pointer colorTable)  throws NIVisionException\{\par
        Pointer p = new Pointer(fileName.length() + 1);\par
        p.setString(0, fileName);\par
        setWriteFileAllowed(true);\par
        try \{\par
            assertCleanStatus(imaqWriteFileFn.call3(image, p, colorTable)); //zero is unused color table\par
        \} finally \{\par
            p.free();\par
        \}\par
    \}\par
\par
    private static final BlockingFunction imaqReadFileFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqReadFile");\par
    static \{ imaqReadFileFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Read an image from to the given image from the given filename\par
     * @param image The image to store the data in.\par
     * @param fileName The name of the file to load.\par
     */\par
    public static void readFile(Pointer image, String fileName)  throws NIVisionException\{\par
        Pointer p = new Pointer(fileName.length() + 1);\par
        p.setString(0, fileName);\par
        try \{\par
            assertCleanStatus(imaqReadFileFn.call4(image, p, 0, 0)); //zeros are unused color table and num colors\par
        \} finally \{\par
            p.free();\par
        \}\par
    \}\par
\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqCloseAVI(AVISession session);\par
    //IMAQ_FUNC AVISession  IMAQ_STDCALL imaqCreateAVI(const char* fileName, const char* compressionFilter, int quality, unsigned int framesPerSecond, unsigned int maxDataSize);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqGetAVIInfo(AVISession session, AVIInfo* info);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqGetFileInfo(const char* fileName, CalibrationUnit* calibrationUnit, float* calibrationX, float* calibrationY, int* width, int* height, ImageType* imageType);\par
    //IMAQ_FUNC FilterName* IMAQ_STDCALL imaqGetFilterNames(int* numFilters);\par
    //IMAQ_FUNC char**      IMAQ_STDCALL imaqLoadImagePopup(const char* defaultDirectory, const char* defaultFileSpec, const char* fileTypeList, const char* title, int allowMultiplePaths, ButtonLabel buttonLabel, int restrictDirectory, int restrictExtension, int allowCancel, int allowMakeDirectory, int* cancelled, int* numPaths);\par
    //IMAQ_FUNC AVISession  IMAQ_STDCALL imaqOpenAVI(const char* fileName);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqReadAVIFrame(Image* image, AVISession session, unsigned int frameNum, void* data, unsigned int* dataSize);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqReadVisionFile(Image* image, const char* fileName, RGBValue* colorTable, int* numColors);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWriteAVIFrame(Image* image, AVISession session, const void* data, unsigned int dataLength);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWriteBMPFile(const Image* image, const char* fileName, int compress, const RGBValue* colorTable);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWriteJPEGFile(const Image* image, const char* fileName, unsigned int quality, void* colorTable);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWriteJPEG2000File(const Image* image, const char* fileName, int lossless, float compressionRatio, const JPEG2000FileAdvancedOptions* advancedOptions, const RGBValue* colorTable);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWritePNGFile2(const Image* image, const char* fileName, unsigned int compressionSpeed, const RGBValue* colorTable, int useBitDepth);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWriteTIFFFile(const Image* image, const char* fileName, const TIFFFileOptions* options, const RGBValue* colorTable);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqWriteVisionFile(const Image* image, const char* fileName, const RGBValue* colorTable);\par
\par
\par
    //============================================================================\par
    //  Frequency Domain Analysis functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqAttenuate(Image* dest, const Image* source, AttenuateMode highlow);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqConjugate(Image* dest, const Image* source);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqFFT(Image* dest, const Image* source);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqFlipFrequencies(Image* dest, const Image* source);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqInverseFFT(Image* dest, const Image* source);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqTruncate(Image* dest, const Image* source, TruncateMode highlow, float ratioToKeep);\par
\par
    //============================================================================\par
    //  Pixel Manipulation functions\par
    //============================================================================\par
\par
    private static final BlockingFunction imaqExtractColorPlanesFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqExtractColorPlanes");\par
    static \{ imaqExtractColorPlanesFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Extract the color planes from the given source image into the given planes\par
     * @param source The color source image.\par
     * @param mode The color space to extract the planes of.\par
     * @param plane1 MonoImage destination for the first plane.\par
     * @param plane2 MonoImage destination for the first plane.\par
     * @param plane3 MonoImage destination for the first plane.\par
     */\par
    public static void extractColorPlanes(Pointer source, ColorMode mode, Pointer plane1, Pointer plane2, Pointer plane3)  throws NIVisionException\{\par
        int plane_1 = 0;\par
        int plane_2 = 0;\par
        int plane_3 = 0;\par
        if (plane1 != null)\par
            plane_1 = plane1.address().toUWord().toPrimitive();\par
        if (plane2 != null)\par
            plane_2 = plane2.address().toUWord().toPrimitive();\par
        if (plane3 != null)\par
            plane_3 = plane3.address().toUWord().toPrimitive();\par
        assertCleanStatus(imaqExtractColorPlanesFn.call5(source, mode.value, plane_1, plane_2, plane_3));\par
    \}\par
\par
    private static final BlockingFunction imaqReplaceColorPlanesFn = NativeLibrary.getDefaultInstance().getBlockingFunction("imaqReplaceColorPlanes");\par
    static \{ imaqReplaceColorPlanesFn.setTaskExecutor(taskExecutor); \}\par
\par
    /**\par
     * Replaces one or more of the color planes of a color image. The plane you\par
     * replace may be independent of the image type. For example, you can\par
     * replace the green plane of an RGB image or the hue plane of an HSL image.\par
     * @param dest The destination image.\par
     * @param source The source image.\par
     * @param mode The color space in which the function replaces planes.\par
     * @param plane1 The first plane of replacement data. Set this parameter to null if you do not want to change the first plane of the source image.\par
     * @param plane2 The second plane of replacement data. Set this parameter to null if you do not want to change the second plane of the source image.\par
     * @param plane3 The third plane of replacement data. Set this parameter to null if you do not want to change the third plane of the source image.\par
     */\par
    public static void replaceColorPlanes(Pointer dest, Pointer source,\par
            ColorMode mode, Pointer plane1, Pointer plane2, Pointer plane3)  throws NIVisionException\{\par
        int plane_1 = 0;\par
        int plane_2 = 0;\par
        int plane_3 = 0;\par
        if (plane1 != null)\par
            plane_1 = plane1.address().toUWord().toPrimitive();\par
        if (plane2 != null)\par
            plane_2 = plane2.address().toUWord().toPrimitive();\par
        if (plane3 != null)\par
            plane_3 = plane3.address().toUWord().toPrimitive();\par
        assertCleanStatus(imaqReplaceColorPlanesFn.call6(\par
                dest.address().toUWord().toPrimitive(),\par
                source.address().toUWord().toPrimitive(),\par
                mode.value,\par
                plane_1,\par
                plane_2,\par
                plane_3));\par
    \}\par
\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqArrayToComplexPlane(Image* dest, const Image* source, const float* newPixels, ComplexPlane plane);\par
    //IMAQ_FUNC float* IMAQ_STDCALL imaqComplexPlaneToArray(const Image* image, ComplexPlane plane, Rect rect, int* columns, int* rows);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqExtractComplexPlane(Image* dest, const Image* source, ComplexPlane plane);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqFillImage(Image* image, PixelValue value, const Image* mask);\par
    //IMAQ_FUNC void*  IMAQ_STDCALL imaqGetLine(const Image* image, Point start, Point end, int* numPoints);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqGetPixel(const Image* image, Point pixel, PixelValue* value);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqReplaceComplexPlane(Image* dest, const Image* source, const Image* newValues, ComplexPlane plane);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetLine(Image* image, const void* array, int arraySize, Point start, Point end);\par
    //IMAQ_FUNC int    IMAQ_STDCALL imaqSetPixel(Image* image, Point coord, PixelValue value);\par
\par
    //============================================================================\par
    //  LCD functions\par
    //============================================================================\par
    //IMAQ_FUNC int        IMAQ_STDCALL imaqFindLCDSegments(ROI* roi, const Image* image, const LCDOptions* options);\par
    //IMAQ_FUNC LCDReport* IMAQ_STDCALL imaqReadLCD(const Image* image, const ROI* roi, const LCDOptions* options);\par
\par
    //============================================================================\par
    //  Regions of Interest Manipulation functions\par
    //============================================================================\par
    //IMAQ_FUNC ROI*        IMAQ_STDCALL imaqMaskToROI(const Image* mask, int* withinLimit);\par
    //IMAQ_FUNC ROIProfile* IMAQ_STDCALL imaqROIProfile(const Image* image, const ROI* roi);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqROIToMask(Image* mask, const ROI* roi, int fillValue, const Image* imageModel, int* inSpace);\par
    //IMAQ_FUNC int         IMAQ_STDCALL imaqTransformROI2(ROI* roi, const CoordinateSystem* baseSystem, const CoordinateSystem* newSystem);\par
\par
    //============================================================================\par
    //  OCR functions\par
    //============================================================================\par
    //IMAQ_FUNC CharSet*         IMAQ_STDCALL imaqCreateCharSet();\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqDeleteChar(CharSet* set, int index);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqGetCharCount(const CharSet* set);\par
    //IMAQ_FUNC CharInfo2*       IMAQ_STDCALL imaqGetCharInfo2(const CharSet* set, int index);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqReadOCRFile(const char* fileName, CharSet* set, String255 setDescription, ReadTextOptions* readOptions, OCRProcessingOptions* processingOptions, OCRSpacingOptions* spacingOptions);\par
    //IMAQ_FUNC ReadTextReport3* IMAQ_STDCALL imaqReadText3(const Image* image, const CharSet* set, const ROI* roi, const ReadTextOptions* readOptions, const OCRProcessingOptions* processingOptions, const OCRSpacingOptions* spacingOptions);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqRenameChar(CharSet* set, int index, const char* newCharValue);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqSetReferenceChar(const CharSet* set, int index, int isReferenceChar);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqTrainChars(const Image* image, CharSet* set, int index, const char* charValue, const ROI* roi, const OCRProcessingOptions* processingOptions, const OCRSpacingOptions* spacingOptions);\par
    //IMAQ_FUNC int*             IMAQ_STDCALL imaqVerifyPatterns(const Image* image, const CharSet* set, const String255* expectedPatterns, int patternCount, const ROI* roi, int* numScores);\par
    //IMAQ_FUNC int*             IMAQ_STDCALL imaqVerifyText(const Image* image, const CharSet* set, const char* expectedString, const ROI* roi, int* numScores);\par
    //IMAQ_FUNC int              IMAQ_STDCALL imaqWriteOCRFile(const char* fileName, const CharSet* set, const char* setDescription, const ReadTextOptions* readOptions, const OCRProcessingOptions* processingOptions, const OCRSpacingOptions* spacingOptions);\par
\par
    //============================================================================\par
    //  Classification functions\par
    //============================================================================\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqAddClassifierSample(Image* image, ClassifierSession* session, const ROI* roi, const char* sampleClass, double* featureVector, unsigned int vectorSize);\par
    //IMAQ_FUNC ClassifierReport*              IMAQ_STDCALL imaqClassify(Image* image, const ClassifierSession* session, const ROI* roi, double* featureVector, unsigned int vectorSize);\par
    //IMAQ_FUNC ClassifierSession*             IMAQ_STDCALL imaqCreateClassifier(ClassifierType type);\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqDeleteClassifierSample(ClassifierSession* session, int index);\par
    //IMAQ_FUNC ClassifierAccuracyReport*      IMAQ_STDCALL imaqGetClassifierAccuracy(const ClassifierSession* session);\par
    //IMAQ_FUNC ClassifierSampleInfo*          IMAQ_STDCALL imaqGetClassifierSampleInfo(const ClassifierSession* session, int index, int* numSamples);\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqGetNearestNeighborOptions(const ClassifierSession* session, NearestNeighborOptions* options);\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqGetParticleClassifierOptions(const ClassifierSession* session, ParticleClassifierPreprocessingOptions* preprocessingOptions, ParticleClassifierOptions* options);\par
    //IMAQ_FUNC ClassifierSession*             IMAQ_STDCALL imaqReadClassifierFile(ClassifierSession* session, const char* fileName, ReadClassifierFileMode mode, ClassifierType* type, ClassifierEngineType* engine, String255 description);\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqRelabelClassifierSample(ClassifierSession* session, int index, const char* newClass);\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqSetParticleClassifierOptions(ClassifierSession* session, const ParticleClassifierPreprocessingOptions* preprocessingOptions, const ParticleClassifierOptions* options);\par
    //IMAQ_FUNC NearestNeighborTrainingReport* IMAQ_STDCALL imaqTrainNearestNeighborClassifier(ClassifierSession* session, const NearestNeighborOptions* options);\par
    //IMAQ_FUNC int                            IMAQ_STDCALL imaqWriteClassifierFile(const ClassifierSession* session, const char* fileName, WriteClassifierFileMode mode, const String255 description);\par
\par
    //============================================================================\par
    //  Inspection functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqCompareGoldenTemplate(const Image* image, Image* goldenTemplate, Image* brightDefects, Image* darkDefects, const InspectionAlignment* alignment, const InspectionOptions* options);\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqLearnGoldenTemplate(Image* goldenTemplate, PointFloat originOffset, const Image* mask);\par
\par
    //============================================================================\par
    //  Morphology functions\par
    //============================================================================\par
    //IMAQ_FUNC int IMAQ_STDCALL imaqGrayMorphology(Image* dest, Image* source, MorphologyMethod method, const StructuringElement* structuringElement);\par
\par
\}\par
}
 